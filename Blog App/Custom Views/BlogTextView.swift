//
//  AutoHeightTextView.swift
//  Blog App
//
//  Created by Van Luu on 10/10/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class BlogTextView: UITextView {
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    isScrollEnabled = false
  }
  
}
