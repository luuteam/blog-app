//
//  DetailBlogTableView.swift
//  Blog App
//
//  Created by Van Luu on 10/14/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import Kingfisher
import SKPhotoBrowser

protocol DetailBlogTableViewDelegate: class {
  func scrollViewDidScrollUp()
  func scrollViewDidScrollDown()
  func presentPhotoBrowser(viewController: UIViewController)
}

class DetailBlogTableView: UITableView {
  
  // MARK: Variables

  var text: String = "" {
    didSet {
      configureText()
    }
  }
  
  var imageHeaderView: UIImageView!
  var imageHeaderViewHeight: CGFloat!
  var browser: SKPhotoBrowser!
  
  var arrParagraph = [String]()
  var lastContentOffsetY: CGFloat = 0
  var isScrollingUp = false
  weak var controllerDelegate: DetailBlogTableViewDelegate!
  
  lazy var testLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.lineBreakMode = .byWordWrapping
    return label
  }()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    delegate = self
    dataSource = self
    
    self.rowHeight = UITableViewAutomaticDimension
    self.estimatedRowHeight = 150.0
    
    // get headerView
    if let headerView = self.tableHeaderView as? UIImageView {
      self.imageHeaderView = headerView
      self.tableHeaderView = nil
      self.addSubview(imageHeaderView)
    }
  }

}

// MARK: Function
extension DetailBlogTableView {
  func configureText() {
    if arrParagraph.count == 0 {
      arrParagraph = text.components(separatedBy: "\\l")
      
      
      // config header view
      configHeaderView(text: arrParagraph.first!)
      setHeaderViewContent(arrParagraph.first!)
      
      // remove first element
      arrParagraph.remove(at: 0)
      
      // reload tableview data
      reloadData()
      
      // udate content inset and content offset
      updateTableViewContentInset()
    }
  }
  
  // MARK: Method
  
  // MARK: Header view method
  
  func configHeaderView(text: String) {
    if text.isMatchRegex(regex: RegularRegex.image) {
      imageHeaderViewHeight = heightForImagePattern(text: text)
      imageHeaderView.frame.size = CGSize(width: self.bounds.width, height: imageHeaderViewHeight)
    }
  }
  
  func updateTableViewContentInset() {
    self.contentInset = UIEdgeInsets(top: imageHeaderViewHeight, left: 0, bottom: 0, right: 0)
    self.contentOffset = CGPoint(x: 0, y: -imageHeaderViewHeight)
    
    updateHeaderView()
  }
  
  func updateHeaderView() {
    
    var headerRect = CGRect(x: 0, y: -imageHeaderViewHeight, width: self.bounds.width, height: imageHeaderViewHeight)
    if self.contentOffset.y < -imageHeaderViewHeight {
      headerRect.origin.y = self.contentOffset.y
      headerRect.size.height = -self.contentOffset.y
    }
    
    imageHeaderView.frame = headerRect
  }
  
  func setHeaderViewContent(_ text: String) {
    if let url = urlImageFromText(text) {
      let resource = URL(string: url)
      imageHeaderView.kf.setImage(with: resource)
    }
  }
  
  func urlImageFromText(_ text: String) -> String? {
    let textWithoutTag = text.replacingOccurrences(of: "<i>", with: "")
    let arrText = textWithoutTag.components(separatedBy: "|")
    
    if arrText.count > 0 {
      return arrText[0]
    }
    
    return nil
    
  }
  
  
  // MARK: Calculate height method
  
  func heightFor(text: String) -> CGFloat {
    let attributeText = Appearance.attributedStringWithTag(tags: ("<b>", "</b>"), text: text)
    let rect = attributeText!.boundingRect(with: CGSize(width: self.bounds.width - 10.0, height: 10000), options: [NSStringDrawingOptions.usesLineFragmentOrigin, NSStringDrawingOptions.usesFontLeading], context: nil)
//    print("rect \(rect)")
    return rect.height
  }
  
  func heightForImagePattern(text: String) -> CGFloat {
    
    let textWithoutTag = text.replacingOccurrences(of: "<i>", with: "")
    let arrText = textWithoutTag.components(separatedBy: "|")
    
    let imageSize = CGSize(width: arrText[1].cgFloat, height: arrText[2].cgFloat)
    let aspectFillImageHeight = CGSize.aspectFitWithWidth(aspectRatio: imageSize, boundingSize: CGSize(width: self.bounds.width, height: 200.0))
    
    // image with note
    if arrText.count == 4 {
      let noteHeight = heightForNoteText(text: arrText[3])
      return aspectFillImageHeight + noteHeight
    }
    
    return aspectFillImageHeight
  }
  
  func heightForTitleParagraph(text: String) -> CGFloat {
    testLabel.attributedText = applyAttributeText(text: text, withAttribute: Appearance.attributeTitleText)
    return testLabel.sizeThatFits(CGSize(width: self.bounds.width, height: 10000)).height + 50.0
  }
  
  func heightForNoteText(text: String) -> CGFloat {
    testLabel.attributedText = applyAttributeText(text: text, withAttribute: Appearance.attributeNoteText)
    return testLabel.sizeThatFits(CGSize(width: self.bounds.width, height: 10000)).height
  }
  
  func applyAttributeText(text: String, withAttribute attribute: [String : AnyObject]) -> NSMutableAttributedString {
    return NSMutableAttributedString(string: text, attributes: attribute)
  }
}


// MARK:  UITableViewDataSource
extension DetailBlogTableView: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return arrParagraph.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let text = arrParagraph[indexPath.row]
    
    if text.isMatchRegex(regex: RegularRegex.image) {
      let cell = tableView.dequeueReusableCell(type: DetailBlogImageTableViewCell.self)!
      cell.configureImageNote(text: text)
      cell.delegate = self
      
      return cell
      
    } else if text.contains("<t>") && text.contains("</t>") {
      
      let cell = tableView.dequeueReusableCell(type: DetailBlogTitleTableViewCell.self)!
      cell.configureTitle(text)
      
      return cell
      
    } else if text.isMatchRegex(regex: RegularRegex.relatedImage) {
      
      let cell = tableView.dequeueReusableCell(type: RelatedImageTableViewCell.self)!
      cell.configureRelatedImage(contentWithTag: text)
      cell.delegate = self
      
      return cell
      
    } else {
      let cell = tableView.dequeueReusableCell(type: DetailBlogTextTableViewCell.self)!
      var regex: (String, String)!
      regex = ("<b>", "</b>")
      
      cell.configureText(text: text, regex: regex)

      return cell
    }

  }
  
}

// MARK: UITableViewDelegate
extension DetailBlogTableView: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    let text = arrParagraph[indexPath.row]
    
    if text.isMatchRegex(regex: RegularRegex.image) {
      return heightForImagePattern(text: text)
      
    } else if text.contains("<t>") && text.contains("</t>") {
      return heightForTitleParagraph(text: text)
      
    } else {
      return self.rowHeight
    }
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    updateHeaderView()
    
    let contentOffsetY = scrollView.contentOffset.y
    
    if lastContentOffsetY >= contentOffsetY && isScrollingUp == false {
      controllerDelegate.scrollViewDidScrollUp()
      isScrollingUp = true
    } else if lastContentOffsetY < contentOffsetY && isScrollingUp == true {
      controllerDelegate.scrollViewDidScrollDown()
      isScrollingUp = false
    }
    
    lastContentOffsetY = contentOffsetY
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let cell = tableView.cellForRow(at: indexPath) as? DetailBlogImageTableViewCell {
      cell.showImagePreview()
    }
    tableView.deselectRow(at: indexPath, animated: true)
  }

}

// MARK: RelatedTableViewCellDelegate
extension DetailBlogTableView: RelatedImageTableViewCellDelegate {
  func presentPreviewImaController(controller: UIViewController) {
    controllerDelegate.presentPhotoBrowser(viewController: controller)
  }
}
