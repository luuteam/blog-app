//
//  HourGlassIndicatorView.swift
//  Blog App
//
//  Created by Van Luu on 10/24/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class HourGlassIndicatorView: UIView {
  
  var containerLayer: CALayer!
  var topLayer: CAShapeLayer!
  var bottomLayer: CAShapeLayer!
  var lineLayer: CAShapeLayer!
  var textLab: UILabel!
  var topAnimation: CAKeyframeAnimation!
  var bottomAnimation: CAKeyframeAnimation!
  var lineAnimation: CAKeyframeAnimation!
  var containerAnimation: CAKeyframeAnimation!
  
  let kWidth: Double = 40.0
  let duration = 1.5
  var width: Double!
  var height: Double!
  var isShowing = false
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    initLayout()
  }
  
  private func initLayout() {
    if containerLayer != nil {
//      print("not nil")
//      dismiss()
//      runAnimation()
      
      return
    }
    print("nil")
    
    width = sqrt(kWidth*kWidth + kWidth*kWidth)
    height = sqrt((kWidth*kWidth + kWidth*kWidth) - ((width/2) * (width/2)))
    
    initContainerLayerAndLabel()
    initTopLayer()
    initBottomLayer()
    initLineLayer()
    
    initAnimation()
    
    runAnimation()
    
  }
  
  private func initContainerLayerAndLabel() {
    
    // init container Layer
    containerLayer = CALayer()
    containerLayer.frame = CGRect(x: 0, y: 0, width: width, height: height*2)
    containerLayer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
    containerLayer.position = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
    containerLayer.backgroundColor = UIColor.clear.cgColor
    self.layer.addSublayer(containerLayer)
    
  }
  
  private func initTopLayer() {
        // init top layer path
    let topPath = UIBezierPath()
    topPath.move(to: CGPoint(x: 0, y: 0))
    topPath.addLine(to: CGPoint(x: width, y: 0))
    topPath.addLine(to: CGPoint(x: width/2, y: height))
    topPath.addLine(to: CGPoint(x: 0, y: 0))
    topPath.close()
    
    // init top layer
    topLayer = CAShapeLayer()
    topLayer.frame = CGRect(x: 0, y: 0, width: width, height: height)
    topLayer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
    topLayer.position = CGPoint(x: width/2, y: height)
    topLayer.fillColor = AppColor.hourGlass.cgColor
    topLayer.strokeColor = AppColor.hourGlass.cgColor
    topLayer.path = topPath.cgPath
    
    containerLayer.addSublayer(topLayer)
  }
  
  private func initBottomLayer() {
    
    // init top layer path
    let bottomPath = UIBezierPath()
    bottomPath.move(to: CGPoint(x: width/2, y: 0))
    bottomPath.addLine(to: CGPoint(x: 0, y: height))
    bottomPath.addLine(to: CGPoint(x: width, y: height))
    bottomPath.addLine(to: CGPoint(x: width/2, y: 0))
    bottomPath.close()
    
    // init top layer
    bottomLayer = CAShapeLayer()
    bottomLayer.frame = CGRect(x: 0, y: height, width: width, height: height)
    bottomLayer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
    bottomLayer.position = CGPoint(x: width/2, y: height*2)
    bottomLayer.fillColor = AppColor.hourGlass.cgColor
    bottomLayer.strokeColor = AppColor.hourGlass.cgColor
    bottomLayer.path = bottomPath.cgPath
    bottomLayer.transform = CATransform3DMakeScale(0, 0, 0)
    
    containerLayer.addSublayer(bottomLayer)
  }
  
  private func initLineLayer() {
    let linePath = UIBezierPath()
    linePath.move(to: CGPoint(x: width/2, y: 0))
    linePath.addLine(to: CGPoint(x: width/2, y: height))
    
    lineLayer = CAShapeLayer()
    lineLayer.frame = CGRect(x: 0, y: height, width: width, height: height)
    lineLayer.strokeColor = AppColor.hourGlass.cgColor
    lineLayer.lineWidth = 1.0;
    lineLayer.lineJoin = kCALineJoinMiter;
    lineLayer.lineDashPattern = [1,1]
    lineLayer.lineDashPhase = 3.0
    lineLayer.path = linePath.cgPath
    lineLayer.strokeEnd = 0.0
    
    containerLayer.addSublayer(lineLayer)
  }
  
  private func initAnimation() {
    if (true) {
      topAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
      topAnimation.repeatCount = MAXFLOAT
      topAnimation.duration = duration
      topAnimation.keyTimes = [0.0, 0.9, 1.0]
      topAnimation.values = [1.0, 0.0, 0.0]
    }
    if (true) {
      bottomAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
      bottomAnimation.repeatCount = MAXFLOAT
      bottomAnimation.duration = duration
      bottomAnimation.keyTimes = [0.0, 0.9, 1.0]
      bottomAnimation.values = [0.0, 1.0, 1.0]
    }
    if (true) {
      lineAnimation = CAKeyframeAnimation(keyPath: "strokeEnd")
      lineAnimation.repeatCount = MAXFLOAT
      lineAnimation.duration = duration
      lineAnimation.keyTimes = [0.0, 0.1, 0.9, 1.0]
      lineAnimation.values = [0.0, 1.0, 1.0, 1.0]
    }
    if (true) {
      containerAnimation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
//      containerAnimation.timingFunctions = [CAMediaTimingFunction(controlPoints: 0.2, 1.0, 0.8, 0.0)]
      containerAnimation.duration = duration
      containerAnimation.repeatCount = MAXFLOAT
      containerAnimation.keyTimes = [0.8, 1.0]
      containerAnimation.values = [0.0, M_PI]
    }
  }
  
  func runAnimation() {
    topLayer.add(topAnimation, forKey: "TopAnimation")
    bottomLayer.add(bottomAnimation, forKey: "BottomAnimation")
    lineLayer.add(lineAnimation, forKey: "LineAnimation")
    containerLayer.add(containerAnimation, forKey: "ContainerAnimation")
  }
  
  func dismiss() {
    topLayer.removeAllAnimations()
    bottomLayer.removeAllAnimations()
    lineLayer.removeAllAnimations()
    containerLayer.removeAllAnimations()
  }
  
}
