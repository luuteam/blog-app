//
//  ReplyButton.swift
//  Blog App
//
//  Created by Van Luu on 11/1/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class ReplyButton: UIButton {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setupUI()
  }
  
  func setupUI() {
    cornerRadius = 5.0
    borderColor = AppColor.menuText
    borderWidth = 2.0
    layer.backgroundColor = UIColor.white.cgColor
    
    setTitle("phản hồi", for: .normal)
    titleLabel?.font = Appearance.fontForSize(size: 15.0)
    setTitleColor(AppColor.menuText, for: .normal)
  }
  
}
