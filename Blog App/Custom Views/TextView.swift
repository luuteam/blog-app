//
//  TextView.swift
//  Blog App
//
//  Created by Van Luu on 10/12/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class TextView: UILabel {
  
  var paragraph: String = "" {
    didSet {
      configureParagraph(text: paragraph)
    }
  }
  
  var attributeString: NSMutableAttributedString! = nil
  var regex: (String, String)!
  
  func configureParagraph(text: String) {
    
    if regex == nil { return }
    attributeString = Appearance.attributedStringWithTag(tags: regex, text: paragraph)
    
    self.attributedText = attributeString
  }
}
