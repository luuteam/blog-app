//
//  LoadMoreView.swift
//  Blog App
//
//  Created by Van Luu on 10/31/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class LoadMoreView: UIView {
  
  // MARK: Variable
  lazy var circle1: CAShapeLayer = {
    let layer = CAShapeLayer()
    layer.cornerRadius = 5.0
    layer.backgroundColor = AppColor.menuSearch.cgColor
    
    return layer
  }()
  
  lazy var circle2: CAShapeLayer = {
    let layer = CAShapeLayer()
    layer.cornerRadius = 5.0
    layer.backgroundColor = AppColor.menuSearch.cgColor
    
    return layer
  }()
  
  lazy var circle3: CAShapeLayer = {
    let layer = CAShapeLayer()
    layer.cornerRadius = 5.0
    layer.backgroundColor = AppColor.menuSearch.cgColor
    
    return layer
  }()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setupUI()
  }
  
  func setupUI() {
    self.layer.addSublayer(circle1)
    self.layer.addSublayer(circle2)
    self.layer.addSublayer(circle3)
  }
  
  func boundsAnimation(delay: Double) -> CABasicAnimation {
    let anim = CABasicAnimation(keyPath: "transform.translation.y")
    anim.toValue = 10.0
    anim.beginTime = CACurrentMediaTime() + delay
    anim.autoreverses = true
    anim.repeatCount = 200
    anim.duration = 0.3
    
    return anim
  }
  
  func animate() {
    var anim = boundsAnimation(delay: 0)
    circle1.add(anim, forKey: "boundsAnimation")
    
    anim = boundsAnimation(delay: 0.15)
    circle2.add(anim, forKey: "boundsAnimation")
    
    anim = boundsAnimation(delay: 0.3)
    circle3.add(anim, forKey: "boundsAnimation")
  }
  
  func stopAnimate() {
    circle1.removeAllAnimations()
    circle2.removeAllAnimations()
    circle3.removeAllAnimations()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    circle1.frame = CGRect(x: self.bounds.width/2 - 5.0*4 - 5.0, y: self.bounds.midY, width: 5.0*2, height: 5.0*2)
    circle2.frame = CGRect(x: self.bounds.width/2 - 5.0, y: self.bounds.midY, width: 5.0*2, height: 5.0*2)
    circle3.frame = CGRect(x: self.bounds.width/2 + 5.0*4 - 5.0, y: self.bounds.midY, width: 5.0*2, height: 5.0*2)
  }
}
