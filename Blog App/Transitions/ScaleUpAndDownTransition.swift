//
//  ScaleUpAndDownTransition.swift
//  Blog App
//
//  Created by Van Luu on 10/15/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class ScaleUpAndDownTransition: NSObject, UIViewControllerAnimatedTransitioning {
  
  let duration: TimeInterval = 0.4
  var operation: UINavigationControllerOperation = .push
  var snapView: UIView?
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return duration
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard let toView = transitionContext.view(forKey: UITransitionContextViewKey.to),
      let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) else { return }
    
    let containerView = transitionContext.containerView
    if case .push = operation {
      toView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
      containerView.addSubview(toView)
    } else {
      containerView.insertSubview(toView, belowSubview: fromView)
    }
    
    UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: .curveEaseInOut, animations: {
      if case .push = self.operation  {
        toView.transform = CGAffineTransform.identity
      } else {
        fromView.alpha = 0.0
        fromView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
      }
      }, completion: { isFinished in
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
    })

  }

}
