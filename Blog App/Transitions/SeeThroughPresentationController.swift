//
//  SeeThroughPresentationController.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class SeeThroughPresentationController: UIPresentationController {
  override var shouldRemovePresentersView: Bool {
    return false
  }
}
