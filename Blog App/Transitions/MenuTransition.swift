//
//  MenuTransition.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class MenuTransition: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
  
  let duration: TimeInterval = 0.2
  var isPresenting = true
  
  var snapView: UIView?
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return duration
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard let toView = transitionContext.view(forKey: UITransitionContextViewKey.to),
          let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) else { return }
    
    let containerView = transitionContext.containerView
    if isPresenting {
      snapView = fromView.snapshotView(afterScreenUpdates: true)
      toView.alpha = 0.0
      containerView.addSubview(snapView!)
      containerView.addSubview(toView)
    }
    
    UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
      
      if self.isPresenting {
        toView.alpha = 1.0
      } else {
        fromView.alpha = 0.0
      }
      
      }, completion: { isFinished in
        transitionContext.completeTransition(isFinished)
        
        if !self.isPresenting {
          self.snapView!.removeFromSuperview()
        }
    })
    
  }
  
//  func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
//    return SeeThroughPresentationController(presentedViewController: presented, presenting: presenting)
//  }
  
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    isPresenting = false
    return self
  }
  
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    isPresenting = true
    return self
  }
}
