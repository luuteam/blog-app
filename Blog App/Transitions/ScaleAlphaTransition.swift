//
//  ScaleAlphaTransition.swift
//  Blog App
//
//  Created by Dinh Luu on 21/10/2016.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class ScaleAlphaTransition: NSObject, UIViewControllerAnimatedTransitioning {
  
  let duration: TimeInterval = 0.2
  var operation: UINavigationControllerOperation = .push
  var snapView: UIView?
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return duration
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard let toView = transitionContext.view(forKey: UITransitionContextViewKey.to),
      let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) else { return }
    
    let containerView = transitionContext.containerView
    if case .push = operation {
      toView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
      toView.alpha = 0.0
      containerView.addSubview(toView)
    } else {
      containerView.insertSubview(toView, belowSubview: fromView)
    }
    
    UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
      
      if case .push = self.operation  {
        toView.alpha = 1.0
        toView.transform = CGAffineTransform.identity
      } else {
        fromView.alpha = 0.0
        fromView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
      }
      }, completion: { isFinished in
        
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
    })
    
  }
  
}

