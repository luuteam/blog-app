//
//  Protocols.swift
//  Blog App
//
//  Created by Van Luu on 10/15/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

enum FailureCode {
  
}

typealias SuccessHandler = (Void) -> Void
typealias FailureHandler = (String) -> Void

protocol UserModelProtocol {
  func login(_ email: String, _ password: String, onSuccess:  @escaping LoginSuccessHandler, onFailure: FailureHandler)
  func register(_ email: String, _ password: String, onSuccess: @escaping LoginSuccessHandler, onFailure: FailureHandler)
}

protocol AnimationNavigatorController {
  
}

extension AnimationNavigatorController where Self: UIViewController {
  func setNavigationControllerDelegate() {
    navigationController?.delegate = FlowManager.shareInstance
  }
}


protocol NavigationControllerAppearanceContext: class {
  
  func prefersNavigationControllerBarHidden(navigationController: UINavigationController) -> Bool
  func prefersNavigationControllerToolbarHidden(navigationController: UINavigationController) -> Bool
  func preferredNavigationControllerAppearance(navigationController: UINavigationController) -> Appearance?
  
//  func setNeedsUpdateNavigationControllerAppearance()
  
}

extension NavigationControllerAppearanceContext {
  
  func prefersNavigationControllerBarHidden(navigationController: UINavigationController) -> Bool {
    return true
  }
  
  func prefersNavigationControllerToolbarHidden(navigationController: UINavigationController) -> Bool {
    return true
  }
  
  func preferredNavigationControllerAppearance(navigationController: UINavigationController) -> Appearance? {
    return nil
  }
  
//  func setNeedsUpdateNavigationControllerAppearance() {
//    if let viewController = self as? UIViewController,
//      navigationController = viewController.navigationController as? AppearanceNavigationController {
//      navigationController.updateAppearanceForViewController(viewController)
//    }
//  }
  
}
