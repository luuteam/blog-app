//
//  AboutUsViewController.swift
//  Blog App
//
//  Created by Dinh Luu on 11/11/2016.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  // MARK: IBAction
  @IBAction func back(sender: UIButton) {
    FlowManager.shareInstance.exitCurrentVC()
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
}
