//
//  ImageChooseViewController.swift
//  Blog App
//
//  Created by Van Luu on 10/29/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

protocol ImageChooseViewControllerDelegate: class {
  func didChooseImage(_ url: String)
}

class ImageChooseViewController: UIViewController {
  
  // MARK: IBOutlet
  @IBOutlet weak var topView: UIView!
  @IBOutlet weak var backBut: UIButton!
  @IBOutlet weak var titleLab: UILabel!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var errorLab: UILabel!
  @IBOutlet weak var loadingView: UIActivityIndicatorView!
  
  // MARK: Variables
  var modelController: ImageChooseModelController!
  weak var delegate: ImageChooseViewControllerDelegate!
  var timer: Timer!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    modelController = ImageChooseModelController()
    setupUI()
    
    initTimeAndAttach()
    modelController.getArrayImageURL(completionHandler: {
      self.invalidateTimer()
      self.reloadData()
      }, failureHandler: { msg in
        self.handleError(msg: msg)
    })
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    setupCollectionViewLayout()
  }
  
  // MARK: Method
  func setupCollectionViewLayout() {
    let layoutManager = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    layoutManager.itemSize = CGSize(width: view.bounds.width/3, height: view.bounds.width/3)
    layoutManager.minimumLineSpacing = 0
    layoutManager.minimumInteritemSpacing = 0
  }
  
  func setupUI() {
    topView.backgroundColor = AppColor.menuSearch
    
    collectionView.dataSource = self
    collectionView.delegate = self
  }
  
  func reloadData() {
    collectionView.reloadData()
  }
  
  func handleError(msg: String) {
    loadingView.isHidden = true
    collectionView.isHidden = true
    errorLab.isHidden = false
    errorLab.text = msg
  }
  
  func initTimeAndAttach() {
    timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(stopGetListItem), userInfo: nil, repeats: false)
  }
  
  func stopGetListItem() {
    print("stoppppp")
    modelController.stopGetList {
      self.handleError(msg: "kết nối mạng có vấn đề, xin hãy kiểm tra lại.")
    }
  }
  
  func invalidateTimer() {
    timer.invalidate()
    loadingView.isHidden = true
  }
  
  // MARK: IBAction
  @IBAction func exitImageChoose(sender: UIButton) {
    FlowManager.dismissViewController(from: self)
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
}

// MARK: UICollectionViewDataSource
extension ImageChooseViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return modelController.numberOfRow()
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(type: ImageChooseCollectionViewCell.self, forIndexPath: indexPath)!
    
    let imageURL = modelController.item(atIndex: indexPath.item)
    cell.configureImage(imageURL.url)
    
    return cell
  }
}

// MARK: UICollectionViewDelegate
extension ImageChooseViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let url = modelController.item(atIndex: indexPath.item).url
    delegate.didChooseImage(url!)
  }
}
