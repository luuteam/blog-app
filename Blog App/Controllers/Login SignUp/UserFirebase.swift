//
//  UserFirebase.swift
//  Blog App
//
//  Created by Van Luu on 10/18/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import Foundation
import Firebase

typealias LoginSuccessHandler = (UserInfo) -> Void

class UserFirebase {
  
  private var firebaseURL = ""
  
  func login(_ email: String, _ password: String, onSuccess:  @escaping LoginSuccessHandler, onFailure: FailureHandler) {
    FIRAuth.auth()?.signIn(withEmail: email, password: password) { (user, error) in
      
      if error != nil {
        print(error?.localizedDescription)
      } else {
        let user = UserInfo(email: user!.email!, displayName: user!.displayName!, uID: user!.uid)
        onSuccess(user)
      }
    }
  }
  
  func register(_ email: String, _ password: String, _ userName: String, onSuccess: @escaping LoginSuccessHandler, onFailure: FailureHandler) {
    FIRAuth.auth()?.createUser(withEmail: email, password: password) { (user, error) in
      
      if error != nil {
        print(error?.localizedDescription)
      } else {
        let user = UserInfo(email: user!.email!, displayName: userName, uID: user!.uid)
        onSuccess(user)
        
        let userUpdate = FIRAuth.auth()?.currentUser
        if let user = userUpdate {
          let changeRequest = user.profileChangeRequest()
          
          changeRequest.displayName = userName
          changeRequest.commitChanges { error in
            if let error = error {
              print(error.localizedDescription)
            } else {
              // Profile updated.
            }
          }
        }
        
      }
    }
  }
  
  func logOut() {
    try! FIRAuth.auth()!.signOut()
  }
}
