//
//  UserModelController.swift
//  Blog App
//
//  Created by Van Luu on 10/18/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import Foundation

class UserModelController {
  
  private static var userModel = UserFirebase()
  
  static func login(user: UserLoginSignUp, onSuccess: @escaping SuccessHandler, onFailure: FailureHandler) {
    userModel.login(user.email, user.password, onSuccess: { (user) in
      
      User.save(user: user)
      onSuccess()
      }) { (message) in
        
      
    }
  }
  
  static func register(user: UserLoginSignUp, onSuccess: @escaping SuccessHandler, onFailure: FailureHandler) {
    userModel.register(user.email, user.password, user.userName, onSuccess: { (user) in
      
      User.save(user: user)
      onSuccess()
      }) { (message) in
        
    }
  }
}
