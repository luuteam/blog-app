//
//  ImageChooseCollectionViewCell.swift
//  Blog App
//
//  Created by Van Luu on 10/29/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import Kingfisher

class ImageChooseCollectionViewCell: UICollectionViewCell {
  
  // MARK: IBOutlet
  @IBOutlet weak var imageIma: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    imageIma.backgroundColor = AppColor.blogContent
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
    imageIma.image = nil
  }
  
  func configureImage(_ url: String) {
    let resource = URL(string: url)
    imageIma.kf.setImage(with: resource)
    
    imageIma.backgroundColor = UIColor.clear
  }
}
