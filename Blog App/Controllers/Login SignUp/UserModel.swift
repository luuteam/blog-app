//
//  UserModel.swift
//  Blog App
//
//  Created by Van Luu on 10/18/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import Foundation

struct UserLoginSignUp {
  var email: String!
  var password: String!
  var userName: String = ""
  var userAvatar: String = ""
  
  func isDataInvalid(isLogin: Bool = true) -> Bool {
    
    if isLogin && password.isPasswordValid {
      return true
    } else if !isLogin && password.isPasswordValid && userName != "" {
      return true
    } else {
      return false
    }
  }
}

struct UserInfo {
  var email: String!
  var displayName: String!
  var uID: String!
  
  func toDictionary() -> [String : String] {
    return [
      UserKey.email : email,
      UserKey.displayName : displayName,
      UserKey.uID : uID
    ]
  }
}
