//
//  ImageChooseModelController.swift
//  Blog App
//
//  Created by Van Luu on 10/29/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

struct AvatarModel: ModelProtocol {
  var url: String!
  
  static func parseFromJSON(jsonData: [String : AnyObject]) -> [AvatarModel] {
    var arrImageURL = [AvatarModel]()
    
    for (_, value) in jsonData {
      let avatar = AvatarModel(url: value as! String)
      arrImageURL.append(avatar)
    }
    
    return arrImageURL
  }
}

class ImageChooseModelController {
  var currentArrImageURL: [AvatarModel]!
  var model: FirebaseModel!
  
  init() {
    currentArrImageURL = [AvatarModel]()
    model = FirebaseModel(child: "avatar")
  }
  
  func getArrayImageURL(completionHandler: @escaping (Void) -> Void, failureHandler: @escaping (String) -> Void) {
    let condition = Condition()
    
    model.itemsWithCondition(condition: condition, withSuccess: { (arrImage : [AvatarModel]) in
      
      self.currentArrImageURL = arrImage
      completionHandler()
      
      }, withFailure: { (msg) in
        
        let msgStr = self.getReadableErrorString(error: msg)
        failureHandler(msgStr)
    })
  }
  
  func numberOfRow() -> Int {
    return currentArrImageURL.count
  }
  
  func item(atIndex index: Int) -> AvatarModel {
    return currentArrImageURL[index]
  }
  
  private func getReadableErrorString(error: String) -> String {
    if error == "There is no user record corresponding to this identifier. The user may have been deleted." || error == "The password is invalid or the user does not have a password." {
      
      return "người dùng không tồn tại, có thể do sai email hoặc mật khẩu"
      
    } else if error == "Network error (such as timeout, interrupted connection or unreachable host) has occurred." {
      return "kết nối mạng có vấn đề, xin hãy kiểm tra lại."
    } else {
      return "có vấn đề với việc đăng nhập, thử lại sau."
    }
  }
  
  func stopGetList(completionHandler: (Void) -> Void) {
    model.removeCurrentHandler()
    completionHandler()
  }
  
}
