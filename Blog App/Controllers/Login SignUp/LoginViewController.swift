//
//  LoginViewController.swift
//  Blog App
//
//  Created by Van Luu on 10/16/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, AnimationNavigatorController, NavigationControllerAppearanceContext {
  
  // MARK: IBOutlet
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var emailTef: UITextField!
  @IBOutlet weak var passwordTef: UITextField!
  @IBOutlet weak var loginBut: UIButton!
  @IBOutlet weak var noticeLab: UILabel!
  @IBOutlet weak var stackView: UIStackView!
  
  // MARK: Variables
  var isAbleToLogin = true
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setNavigationControllerDelegate()
    animateContainerView()
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  // MARK: Method
  func animateContainerView() {
    containerView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    
    UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: .curveEaseInOut, animations: {
      
      self.containerView.transform = CGAffineTransform.identity
      }, completion: { _ in })
  }
  
  func loginUser(email: String, password: String) {
    UserManager.loginUser(email: email, password: password, handler: { msg in
    
      if let msg = msg {
        
        self.animateLoginButton(forward: false) {
          self.isAbleToLogin = true
          
          self.noticeLab.isHidden = false
          self.noticeLab.text = msg
        }
        
      } else {
        self.isAbleToLogin = true
        FlowManager.shareInstance.setRootVC(userLoggedIn: true)
      }
    
    })
  }
  
  func animateLoginButton(forward: Bool, handler: ((Void) -> Void)? = nil) {
    let size = forward ? CGSize(width: 40, height: 40) : CGSize(width: containerView.bounds.width - 64, height: 40)
    let xOrigin = forward ? stackView.bounds.width/2 - 20.0 : 0.0
    let background = forward ? AppColor.green : AppColor.menuSearch
    
    if !forward {
      loginBut.setTitle("đăng nhập", for: .normal)
    } else {
      loginBut.setTitle("", for: .normal)
    }

    UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: .curveEaseIn, animations: {
      
      self.loginBut.frame.size = size
      self.loginBut.frame.origin.x = xOrigin
      self.loginBut.backgroundColor = background
      
      }, completion: { _ in
        
        handler?()
    })
  }
  
  // MARK: IBAction
  @IBAction func closeLogin(sender: UIButton) {
    FlowManager.exitLoginSignUp(menuVC: self)
  }
  
  @IBAction func login(sender: UIButton) {
    
    if !isAbleToLogin { return }
    
    view.endEditing(true)
    noticeLab.isHidden = true
    isAbleToLogin = false
    
    guard let email = emailTef.text else { return }
    guard let password = passwordTef.text else { return }
    
    if email == "" || password == "" {
      isAbleToLogin = true
    } else {
      
      animateLoginButton(forward: true) {
        self.loginUser(email: email, password: password)
      }
      
    }
  }

}
