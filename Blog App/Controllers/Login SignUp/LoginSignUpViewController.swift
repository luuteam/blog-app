//
//  LoginSignUpViewController.swift
//  Blog App
//
//  Created by Van Luu on 10/16/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class LoginSignUpViewController: UIViewController, AnimationNavigatorController, NavigationControllerAppearanceContext {
  
  // MARK: IBOutlet
  @IBOutlet weak var loginBut: UIButton!
  @IBOutlet weak var signUpBut: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setNavigationControllerDelegate()
  }
  
  // MARK: IBAction
  @IBAction func login(sender: UIButton) {
    FlowManager.presentLogin(from: self)
  }
  
  @IBAction func signUp(sender: UIButton) {
    FlowManager.presentSignUp(from: self)
  }
  
}
