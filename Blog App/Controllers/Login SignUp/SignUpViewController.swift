//
//  SignUpViewController.swift
//  Blog App
//
//  Created by Van Luu on 10/16/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import Kingfisher

class SignUpViewController: UIViewController {
  
  // MARK: IBOutlet
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var emailTef: UITextField!
  @IBOutlet weak var passwordTef: UITextField!
  @IBOutlet weak var userNameTef: UITextField!
  @IBOutlet weak var avatarIma: UIImageView!
  @IBOutlet weak var registerBut: UIButton!
  @IBOutlet weak var stackView: UIStackView!
  
  // MARK: Variables
  var userAvatar = "https://firebasestorage.googleapis.com/v0/b/blogapp-b69c0.appspot.com/o/unknown-256.png?alt=media&token=a2459e76-164b-425c-92b7-5839ffd71832"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    animateContainerView()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    avatarIma.layer.cornerRadius = avatarIma.bounds.width/2
  }
  
  // MARK: Method
  func animateContainerView() {
    containerView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    //    containerView.alpha = 0.0
    
    UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: .curveEaseInOut, animations: {
      
      self.containerView.transform = CGAffineTransform.identity
      //      self.containerView.alpha = 1.0
      }, completion: { _ in })
  }
  
  // MARK: IBAction
  @IBAction func closeSignUp(sender: UIButton) {
    FlowManager.exitLoginSignUp(menuVC: self)
  }
  
  @IBAction func signUp(sender: UIButton) {
    
    let email = emailTef.text ?? ""
    
    let password = passwordTef.text ?? ""
    
    let userName = userNameTef.text ?? ""
    
    let userSignUp = UserLoginSignUp(email: email, password: password, userName: userName, userAvatar: userAvatar)
    
    if userSignUp.isDataInvalid(isLogin: false) {
      
      animateRegisterButton(forward: true) {
        self.register(userInfo: userSignUp)
      }
      
    } else {
      
      // show pop up missing information
      
    }
  }
  
  @IBAction func showAvatarAlbum(sender: UIButton) {
    FlowManager.presentImageChoose(from: self)
  }
  
  // MARK: Method
  
  func errorBackground(tag: Int) {
    if let view = self.view.viewWithTag(tag + 1) {
      view.backgroundColor = UIColor.red
    }
  }
  
  func normalBackground(tag: Int) {
    if let view = self.view.viewWithTag(tag + 1) {
      view.backgroundColor = UIColor(colorLiteralRed: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
    }
  }
  
  func animateRegisterButton(forward: Bool, handler: ((Void) -> Void)? = nil) {
    let size = forward ? CGSize(width: 40, height: 40) : CGSize(width: containerView.bounds.width - 64, height: 40)
    let xOrigin = forward ? containerView.bounds.width/2 - 20.0 : 0.0
    let background = forward ? AppColor.green : AppColor.menuSearch
    
    if !forward {
      registerBut.setTitle("đăng ký", for: .normal)
    } else {
      registerBut.setTitle("", for: .normal)
    }
    
    UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: .curveEaseIn, animations: {
      
      self.registerBut.frame.size = size
      self.registerBut.frame.origin.x = xOrigin
      self.registerBut.backgroundColor = background
      
    }, completion: { _ in
      
      handler?()
    })
  }
  
  func register(userInfo: UserLoginSignUp) {
    UserManager.registerUser(userInfo: userInfo) { msg in
      
      // error
      if let _ = msg {
        
        self.animateRegisterButton(forward: false)
        
      } else {
        FlowManager.shareInstance.presentBlogListItemController(from: self)
      }
    }
  }
  
}

// MARK: ImageChooseViewControllerDelegate
extension SignUpViewController: ImageChooseViewControllerDelegate {
  func didChooseImage(_ url: String) {
    userAvatar = url
    
    let resource = URL(string: url)
    avatarIma.kf.setImage(with: resource)
    
    FlowManager.dismissViewController(from: self)
  }
}
