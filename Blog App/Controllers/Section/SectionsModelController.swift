//
//  SectionsModelController.swift
//  Blog App
//
//  Created by Van Luu on 11/12/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import Foundation

class SectionsModelController {
  var model: FirebaseModel!
  var currentSections: [SectionModel]!
  
  init() {
    model = FirebaseModel(child: "series")
    currentSections = [SectionModel]()
    currentSections.append(SectionModel(title: "bài đọc mới", description: "danh sách các bài đọc mới"))
  }
  
  func listSection(fromLastBlogID id: String = "", completionHandler: @escaping (Void) -> Void) {
    
    let condition = Condition()
    
    model.itemsWithCondition(condition: condition, withSuccess: { (listSection : [SectionModel]) in
      
      self.currentSections = listSection
      self.currentSections.insert(SectionModel(title: "bài đọc mới", description: "daah sách các bài đọc mới"), at: 0)
      
      completionHandler()
      
    }, withFailure: { (msg) in
      
    })
  }
  
  func numberOfItem() -> Int {
    return currentSections.count
  }
  
  func item(atIndex index: Int) -> SectionModel {
    return currentSections[index]
  }
}
