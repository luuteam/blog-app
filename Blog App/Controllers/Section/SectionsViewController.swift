//
//  SectionsViewController.swift
//  Blog App
//
//  Created by Van Luu on 11/12/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class SectionsViewController: UIViewController {
  
  // MARK: IBOutlet
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var loadingView: UIActivityIndicatorView!
  @IBOutlet weak var errorLab: UILabel!
  
  // MARK: Variables
  var modelController: SectionsModelController!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    modelController = SectionsModelController()
    tableView.dataSource = self
    tableView.delegate = self
    
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 150.0
    
    modelController.listSection {
      self.reloadData()
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: IBAction
  @IBAction func exit(sender: UIButton) {
    FlowManager.exitSection(menuVC: self)
  }
  
  // MARK: Method
  func reloadData() {
    loadingView.stopAnimating()
    
    if modelController.numberOfItem() > 0 {
      tableView.isHidden = false
      errorLab.isHidden = true
      tableView.reloadData()
    } else {
      tableView.isHidden = true
      errorLab.isHidden = false
    }
  }
  
}

// MARK: UITableViewDataSource
extension SectionsViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return modelController.numberOfItem()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let section = modelController.item(atIndex: indexPath.row)
    let cell = tableView.dequeueReusableCell(type: SectionTableViewCell.self)!
    cell.configureSection(section: section)
    
    return cell
    
  }
}


// MARK: UITableViewDelegate
extension SectionsViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let section = modelController.item(atIndex: indexPath.row)
    
    tableView.deselectRow(at: indexPath, animated: true)
    
    if let parent = self.parent as? BlogListItemsViewController {
      parent.getListItemOfSeries(string: section.title)
    }
    
    FlowManager.exitSection(menuVC: self)
  }
}
