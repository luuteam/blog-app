//
//  SectionModel.swift
//  Blog App
//
//  Created by Van Luu on 11/12/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import Foundation
import ObjectMapper

struct SectionModel:ModelProtocol {
  var title: String!
  var description: String!
  
  static func parseFromJSON(jsonData: [String : AnyObject]) -> [SectionModel] {
    var listSection = [SectionModel]()
    
    for (key, item) in jsonData {
      let section = SectionModel(title: key, description: item as! String)
      
      listSection.append(section)
    }
    
    return listSection
  }
}
