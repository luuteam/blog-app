//
//  SectionTableViewCell.swift
//  Blog App
//
//  Created by Van Luu on 11/12/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class SectionTableViewCell: UITableViewCell {
  
  // MARK: IBOutlet
  @IBOutlet weak var titleLab: UILabel!
  @IBOutlet weak var descriptionLab: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  func configureSection(section: SectionModel) {
    titleLab.text = section.title
    descriptionLab.text = section.description
  }
  
}
