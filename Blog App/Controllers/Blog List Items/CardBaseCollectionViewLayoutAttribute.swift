//
//  CardBaseCollectionViewLayoutAttribute.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class CardBaseCollectionViewLayoutAttribute: UICollectionViewLayoutAttributes {
  var topConstant: CGFloat = -40
  var alphaFactor: CGFloat = 0.0
  var scaleFactor: CGFloat = 1.5
  
  override func copy(with zone: NSZone? = nil) -> Any {
    if let attribute = super.copy(with: zone) as? CardBaseCollectionViewLayoutAttribute {
      attribute.topConstant = self.topConstant
      attribute.alphaFactor = alphaFactor
      attribute.scaleFactor = scaleFactor
      return attribute
    }
    
    return super.copy(with: zone)
  }
  
  override func isEqual(_ object: Any?) -> Bool {
    if let attribute = object as? CardBaseCollectionViewLayoutAttribute {
      if attribute.topConstant == self.topConstant && attribute.alphaFactor == alphaFactor && attribute.scaleFactor == scaleFactor {
        return super.isEqual(object)
      }
    }
    
    return false
  }
}
