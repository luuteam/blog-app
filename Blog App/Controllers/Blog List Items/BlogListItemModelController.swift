//
//  BlogListItemModelController.swift
//  Blog App
//
//  Created by Van Luu on 10/22/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

enum BlogListItemCellType {
  case noData
  case loading
  case blogCell(BlogItemModel)
  case error
}

class BlogListItemModelController {
  var model: FirebaseModel!
  var currentListBlog: [BlogListItemCellType]!
  var currentLastIndex: Int = 100000
  var currentSeries = "bài đọc mới"
  var changeSeries = false
  
  init() {
    model = FirebaseModel(child: "list")
    currentListBlog = [BlogListItemCellType]()
    
    currentListBlog.append(BlogListItemCellType.loading)
  }
  
  func listItem(series: String = "", completionHandler: @escaping (Void) -> Void) {
    var condition = Condition()
    condition.limitAndtoFirst = (limit: 20, isToFirst: false)
    condition.isStarting = false
    condition.sortByChild = "stt"
    condition.queryParam = (key: "stt", value: currentLastIndex)
    
    if changeSeries {
      
      if series != "" {
        condition.queryEqual = (key: "series", value: series)
      }
      
      condition.queryParam = (key: "stt", value: currentLastIndex)
    }
    
    model.itemsWithCondition(condition: condition, withSuccess: { (listBlog : [BlogItemModel]) in
      
      if self.changeSeries {
        self.changeSeries = false
      }
      
      // remove loading data cell at first if exist
      if case BlogListItemCellType.loading = self.currentListBlog[0] {
        self.currentListBlog.remove(at: 0)
      }
      
      // remove loading data cell at last if exist
      if let last = self.currentListBlog.last, case BlogListItemCellType.loading = last {
        self.currentListBlog.removeLast()
      }
      
      let sortedList = (listBlog as [BlogItemModel]).sorted { $0.stt > $1.stt }
      if let stt = sortedList.last?.stt { self.currentLastIndex = stt - 1 }
      
      // append new data
      let sortedSeries = sortedList.filter {
        if self.currentSeries == "bài đọc mới" {
          return true
        } else if $0.series == self.currentSeries {
          return true
        }
        
        return false
      }
      
      self.currentListBlog.append(contentsOf: (sortedSeries.map {
        return BlogListItemCellType.blogCell($0)
      }))
      
      // if no new data
      if listBlog.count == 0 {
        //self.currentListBlog.append(BlogListItemCellType.noData)
      }
      
      // if number of new data % number of current data => may be still have new data => add loading cell
      if self.currentListBlog.count > 0 && self.currentListBlog.count % 5 == 0 {
        self.currentListBlog.append(BlogListItemCellType.loading)
      }
      
      completionHandler()
      
    }, withFailure: { (msg) in
      
    })
  }
  
  func numberOfItem() -> Int {
    return currentListBlog.count
  }
  
  func cellItem(atIndex index: Int) -> BlogListItemCellType {
    return currentListBlog[index]
  }
  
  func stopGetList(completionHandler: (Void) -> Void) {
    model.removeCurrentHandler()
    if case BlogListItemCellType.loading = currentListBlog.last! {
      currentListBlog.removeLast()
      currentListBlog.append(BlogListItemCellType.error)
      completionHandler()
    }
    
  }
  
  func handlerAfterStop() {
    currentListBlog.removeLast()
    currentListBlog.append(BlogListItemCellType.error)
  }
  
  func retry(completionHandler: @escaping (Void) -> Void) {
    if case BlogListItemCellType.error = currentListBlog.last! {
      currentListBlog.removeLast()
      currentListBlog.append(BlogListItemCellType.loading)
    }
    
    listItem {
      completionHandler()
    }
  }
  
  func shouldChangeSeries(series: String) -> Bool {
    if currentSeries != series {
      currentSeries = series
      currentListBlog.removeAll()
      currentListBlog.append(.loading)
      currentLastIndex = 100000
      
      changeSeries = true
    } else {
      changeSeries = false
    }
    
    return changeSeries
  }
}
