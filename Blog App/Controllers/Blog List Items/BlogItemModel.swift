//
//  BlogItemModel.swift
//  Blog App
//
//  Created by Van Luu on 10/22/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import ObjectMapper

struct BlogItemModel: Mappable, ModelProtocol {
  var id: String!
  var title: String!
  var description: String!
  var image: String!
  var series: String!
  var stt: Int!
  
  var summary: String {
    return "<s>\(title!)</s>\n\(description!)"
  }
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    title        <- map["title"]
    description  <- map["description"]
    image        <- map["image"]
    series       <- map["series"]
    stt          <- map["stt"]
  }
  
  static func parseFromJSON(jsonData: [String : AnyObject]) -> [BlogItemModel] {
    var listBlog = [BlogItemModel]()
    
    for (key, item) in jsonData {
      var blog = BlogItemModel(JSON: item as! [String : Any])
      blog?.id = key
      
      if let newBlog = blog {
        listBlog.append(newBlog)
      }
    }
    
    return listBlog
  }
}
