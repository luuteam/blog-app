//
//  BlogItemCollectionViewCell.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import Kingfisher

class BlogItemCollectionViewCell: UICollectionViewCell {
  
  // MARK: IBOutlet
  @IBOutlet weak var colorView: UIView!
  @IBOutlet weak var contentLab: UILabel!
  @IBOutlet weak var blogIma: UIImageView!
  @IBOutlet weak var colorViewTopConstraint: NSLayoutConstraint!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    setupLayout()
  }
  
  func configureBlog(_ content: String, imaURL: String) {
    contentLab.attributedText = Appearance.attributedStringWithTag(tags: ("<s>", "</s>"), text: content)
    let resource = URL(string: imaURL)
    blogIma.kf.setImage(with: resource)
  }
  
  private func setupLayout() {
    let angle = CGFloat((-9).degreesToRadians)
    colorView.layer.transform = CATransform3DMakeRotation(angle, 0, 0, 1)
    contentLab.alpha          = 0.0
  }
  
  override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
    guard let attribute = layoutAttributes as? CardBaseCollectionViewLayoutAttribute else { return }
    
    colorViewTopConstraint.constant = attribute.topConstant
    contentLab.alpha                = attribute.alphaFactor
    contentLab.transform            = CGAffineTransform(scaleX: 1.2 - attribute.scaleFactor, y: 1.2 - attribute.scaleFactor)
  }
}
