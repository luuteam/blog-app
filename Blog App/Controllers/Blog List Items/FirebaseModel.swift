//
//  BlogListItemFirebaseModel.swift
//  Blog App
//
//  Created by Van Luu on 10/22/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import Foundation
import Firebase
import ObjectMapper

protocol ModelProtocol {
  static func parseFromJSON(jsonData: [String : AnyObject]) -> [Self]
}

extension ModelProtocol {
  func jsonToWrite() -> Any {
    return [:]
  }
}

struct Condition {
  var additionChild: String?
  var sortByChild: String?
  var limitAndtoFirst: (limit: UInt, isToFirst: Bool)?
  var queryParam: (key: String, value: Any)?
  var queryEqual: (key: String, value: Any)?
  var isStarting: Bool = true
  var queryByChild: String?
  var queryByChildValue: String?
}


class FirebaseModel {
  var ref: FIRDatabaseReference!
  var query: FIRDatabaseQuery!
  
  init(child: String) {
    ref = FIRDatabase.database().reference().child(child)
  }
  
  func itemsWithCondition<T: ModelProtocol>(condition: Condition, withSuccess: @escaping ([T]) -> Void, withFailure: ((FailureHandler))? = nil) {
    query = ref

    
    if let param = condition.queryEqual {
      
      query = query.queryEnding(atValue: param.value, childKey: param.key)
      
    } else {
      
      if let child = condition.additionChild {
        ref.child(child)
      }
      
      if let sortChild = condition.sortByChild {
        query = ref.queryOrdered(byChild: sortChild)
      }
      
      if let param = condition.queryParam {
        if condition.isStarting {
          
          query = query.queryStarting(atValue: param.value, childKey: param.key)
          
        } else {
          
          query = query.queryEnding(atValue: param.value, childKey: param.key)
          
        }
      }
      
      if let limit = condition.limitAndtoFirst {
        if limit.isToFirst {
          
          query = query.queryLimited(toFirst: limit.limit)
          
        } else {
          
          query = query.queryLimited(toLast: limit.limit)
          
        }
      }
    }
    
    query.observeSingleEvent(of: .value, with: { snapshot in

      guard let listItemJSON = snapshot.value as? [String : AnyObject] else {
        let emptyT = [T]()
        withSuccess(emptyT)
        return
      }
      
      let listItem = T.parseFromJSON(jsonData: listItemJSON)
      withSuccess(listItem)
      
      }, withCancel: { error in
    
        print(error.localizedDescription)
        
    })
  }
  
  func valueOfItemAtPath<T: ModelProtocol>(path: String, withSuccess: @escaping ([T]) -> Void, withFailure: ((FailureHandler))? = nil) {
    ref.child(path).observeSingleEvent(of: .value, with: { snapshot in
      
      guard let itemJSON = snapshot.value as? [String : AnyObject] else { return }
      let item = T.parseFromJSON(jsonData: itemJSON)
      withSuccess(item)
      
      }, withCancel: { error in
    
        withFailure?(error.localizedDescription)
    
    })
  }
  
  func writeOneData(atPath path: String, autoID: Bool, content: [String : Any], withSuccess: @escaping (Void) -> Void, withFailure: (FailureHandler)? = nil) {
    var query: FIRDatabaseReference = ref
    
    if path != "" {
      query = ref.child(path)
    }
    
    if autoID {
      query = query.childByAutoId()
    }
    
    query.setValue(content, withCompletionBlock: { (error, ref) in
      
      if error == nil {
        withSuccess()
        
      } else {
        
        print(error!.localizedDescription)
      }
    })
  }
  
  func removeCurrentHandler() {
    query.removeAllObservers()
  }
  
}
