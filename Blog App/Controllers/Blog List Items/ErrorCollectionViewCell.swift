//
//  ErrorCollectionViewCell.swift
//  Blog App
//
//  Created by Van Luu on 11/6/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class ErrorCollectionViewCell: UICollectionViewCell {
  
  // MARK: IBOutlet
  
  // MARK: Variables
  var handler: ((Void) -> Void)!
  
  
  // MARK: IBAction
  @IBAction func retry(sender: UIButton) {
    handler()
  }
    
}
