//
//  BlogListItemsViewController.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class BlogListItemsViewController: UIViewController, NavigationControllerAppearanceContext {
  
  // MARK: IBOutlet
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var seriesLab: UILabel!
  @IBOutlet weak var sectionBut: UIButton!
  
  // MARK: Variables
  var modelController: BlogListItemModelController!
  var shouldShowLoading = false
  var timer: Timer!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    modelController = BlogListItemModelController()
    
    collectionView.dataSource = self
    collectionView.delegate = self
    
    initTimeAndAttach()
    
    getLIstItem()
    seriesLab.text = "bài đọc mới"
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    let layoutManager = collectionView.collectionViewLayout as! CardBaseCollectionViewFlowLayout
    layoutManager.setupLayout()
  }
  
  // MARK: Method
  
  func getLIstItem() {
    modelController.listItem {
      
      self.timer.invalidate()
      self.reloadData()
    }
  }
  
  func getListItemOfSeries(string: String) {
    
    if modelController.shouldChangeSeries(series: string) {
      seriesLab.text = string
      timer.invalidate()
      initTimeAndAttach()
      
      var series = ""
      if string != "bài đọc mới" {
        series = string
      }
      
      collectionView.reloadData()
      
      DispatchQueue.main.async {
        self.modelController.listItem(series: series) {
          
          self.timer.invalidate()
          self.reloadData()
        }
      }
      
    }
    
  }
  
  func retry() {
    initTimeAndAttach()
    
    modelController.retry {
      self.timer.invalidate()
      self.collectionView.isScrollEnabled = true
      self.reloadData()
    }
    
    // reload without animation
    collectionView.reloadData()
  }
  
  func reloadData() {
    sectionBut.isEnabled = false
    // move left visible cell
    let visibleCell = self.collectionView.visibleCells
    
    for (index, cell) in visibleCell.enumerated() {
      
      UIView.animate(withDuration: 0.5, animations: {
        cell.transform = CGAffineTransform(translationX: -500, y: 0)
        }, completion: { _ in
          
          if index == visibleCell.count - 1 {
            self.collectionView.isHidden = true
            
            // reset transform
            for transformCell in visibleCell {
              transformCell.transform = CGAffineTransform.identity
            }
            
            // update new data
            self.collectionView.reloadData()
            self.animateNewCell()
          }
      })
    }
    
  }
  
  func animateNewCell() {
    
    DispatchQueue.main.async {
      let arrCell = self.collectionView.visibleCells
      for cell in arrCell {
        
        if self.isCellIsGreaterThanCenter(cell: cell) {
          cell.transform = CGAffineTransform(translationX: 500, y: 0)
        }
      }
      
      UIView.animate(withDuration: 0.5, animations: {
        
        self.collectionView.isHidden = false
        
        }, completion: { _ in
          
          for cell in arrCell {
            UIView.animate(withDuration: 1.0) {
              cell.transform = CGAffineTransform(translationX: 0, y: 0)
            }
          }
          
          self.sectionBut.isEnabled = true
          
      })
      
    }
  }
  
  func isCellIsGreaterThanCenter(cell: UICollectionViewCell) -> Bool {
    let centerPoint = self.view.center
    let pointInCollectionView = collectionView.convert(centerPoint, from: self.view)
    guard let indexPathAtCenter = collectionView.indexPathForItem(at: pointInCollectionView) else { return false }
    guard let cellIndexPath = collectionView.indexPath(for: cell) else { return false }
    
    if cellIndexPath.row >= indexPathAtCenter.row { return true }
    
    return false
  }
  
  func initTimeAndAttach() {
    timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(stopGetListItem), userInfo: nil, repeats: false)
  }
  
  func stopGetListItem() {
    print("stoppppp")
    modelController.stopGetList {
      self.collectionView.reloadData()
    }
  }
  
  // MARK: IBAction
  @IBAction func showMenu(sender: UIButton) {
    FlowManager.presentMenu(from: self)
  }
  
  @IBAction func showSection(sender: UIButton) {
    FlowManager.presentSection(from: self)
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
}

// MARK: UICollectionViewDataSource
extension BlogListItemsViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return modelController.numberOfItem()
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cellType = modelController.cellItem(atIndex: indexPath.row)
    switch cellType {
    case .loading:
      let cell = collectionView.dequeueReusableCell(type: BlogItemLoadingCollectionViewCell.self, forIndexPath: indexPath)!
      return cell
      
    case .blogCell(let blog):
      let cell = collectionView.dequeueReusableCell(type: BlogItemCollectionViewCell.self, forIndexPath: indexPath)!
      cell.configureBlog(blog.summary, imaURL: blog.image)
      
      return cell
    case .noData:
      let cell = collectionView.dequeueReusableCell(type: BlogItemLoadingCollectionViewCell.self, forIndexPath: indexPath)!
      
      return cell
    case .error:
      let cell = collectionView.dequeueReusableCell(type: ErrorCollectionViewCell.self, forIndexPath: indexPath)!
      cell.handler = {
        self.retry()
      }
      
      return cell
    }
    
    
  }
}

// MARK: UICollectionViewDelegate

extension BlogListItemsViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if case BlogListItemCellType.blogCell(let blog) = modelController.cellItem(atIndex: indexPath.row) {
      FlowManager.shareInstance.presentBlogDetailController(from: self, blogID: blog.id)
    }
  }
  
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    
    let centerPoint = self.view.center
    let pointInCollectionView = collectionView.convert(centerPoint, from: self.view)
    guard let indexPathAtCenter = collectionView.indexPathForItem(at: pointInCollectionView) else { return }
    let cell = collectionView.cellForItem(at: indexPathAtCenter)
    
    if cell is BlogItemLoadingCollectionViewCell {
      collectionView.isScrollEnabled = false
      
      initTimeAndAttach()
      modelController.listItem {
        self.timer.invalidate()
        self.collectionView.isScrollEnabled = true
        self.reloadData()
      }
    }
  }
}
