//
//  BlogItemLoadingCollectionViewCell.swift
//  Blog App
//
//  Created by Van Luu on 10/24/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class BlogItemLoadingCollectionViewCell: UICollectionViewCell {
  
  // MARK: IBOutlet
  @IBOutlet weak var loadingView: HourGlassIndicatorView!
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
    loadingView.dismiss()
    loadingView.runAnimation()
  }
}
