//
//  BlogRelatedTableViewCell.swift
//  Blog App
//
//  Created by Van Luu on 10/15/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import Kingfisher

class BlogRelatedCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var blogIma: UIImageView!
  @IBOutlet weak var titleLab: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    blogIma.image = nil
    titleLab.text = ""
  }
  
  func configureBlog(imageURL url: String, title: String) {
    let resource = URL(string: url)
    blogIma.kf.setImage(with: resource)
    
    titleLab.text = title
  }
  
}
