//
//  RelatedImageCollectionViewCell.swift
//  Blog App
//
//  Created by Dinh Luu on 10/11/2016.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import Kingfisher
import SKPhotoBrowser

protocol RelatedImageTableViewCellDelegate: class {
  func presentPreviewImaController(controller: UIViewController)
}

class RelatedImageTableViewCell: UITableViewCell {
  
  // MARK: IBOutlet
  @IBOutlet weak var collectionView: UICollectionView!
  
  // MARK: Variables
  typealias RelatedImage = (url: String, note: String?)
  var content: String!
  var arrIma: [RelatedImage] = []
  weak var delegate: RelatedImageTableViewCellDelegate!
  var browser: SKPhotoBrowser!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    collectionView.dataSource = self
    collectionView.delegate = self
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    let layoutManager = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    layoutManager.itemSize = CGSize(width: self.bounds.width/3, height: self.bounds.height)
    layoutManager.minimumLineSpacing = 0
    layoutManager.minimumInteritemSpacing = 0
  }
  
  // MARK: Method
  func configureRelatedImage(contentWithTag: String) {
   
    let content = contentWithTag.replacingOccurrences(of: "<r>", with: "")
    let arrContent = content.components(separatedBy: "|")
    
    if arrContent.count > 0 {
      
      for content in arrContent {
        var image: RelatedImage = (url: "", note: nil)
        var arrIma = content.components(separatedBy: "<note>")
        image.url = arrIma[0]
        
        if arrIma.count > 1 {
          image.note = arrIma[1]
        }
        
        self.arrIma.append(image)
        
      }
     
      initPhotoBrowser(withPhoto: self.arrIma)
      collectionView.reloadData()
    }
  }
  
  private func initPhotoBrowser(withPhoto arrIma: [RelatedImage]) {
    if browser != nil { return }
    
    var images = [SKPhoto]()
    
    for image in arrIma {
      let photo = SKPhoto.photoWithImageURL(image.url)
      photo.shouldCachePhotoURLImage = true
      
      if let note = image.note {
        photo.caption = note
      }
      
      photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
      images.append(photo)
    }
    
    browser = SKPhotoBrowser(photos: images)
  }
  
}


// MARK: UICollectionViewDatasource
extension RelatedImageTableViewCell: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return arrIma.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RelatedImageCell", for: indexPath)
    let image = arrIma[indexPath.item]
    
    if let imaView = cell.viewWithTag(10) as? UIImageView {
      let resource = URL(string: image.url)
      imaView.kf.setImage(with: resource)
    }
    
    return cell
  }
}

// MARK: UICollectionViewDelegate
extension RelatedImageTableViewCell: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    browser.initializePageIndex(indexPath.item)
    
    delegate.presentPreviewImaController(controller: browser)
  }
}
