//
//  BlogDetailViewController.swift
//  Blog App
//
//  Created by Van Luu on 10/10/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class BlogDetailViewController: BaseViewController, AnimationNavigatorController, NavigationControllerAppearanceContext {
  
  // MARK: IBOutlet
  @IBOutlet weak var tableView: DetailBlogTableView!
  @IBOutlet weak var optionView: UIView!
  
  // MARK: Variables
  var blogID: String!
  var modelController: DetailBlogModelController!
  var isEnglish = false
  var lastContentOffsetY: CGFloat = 0.0
  
  lazy var commentRelatedView: CommentRelatedBlogView? = {
    let footerView = CommentRelatedBlogView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.view.bounds.width, height: self.view.bounds.height/2 + 50.0)))
    
    footerView.delegate = self
    return footerView
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    modelController = DetailBlogModelController()
    commentRelatedView?.modelController = self.modelController
    
    tableView.controllerDelegate = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    modelController.blogDetail(withBlogID: blogID, completionHandler: {
      
      self.reloadData()
      }, failureHandler: { msg in
        
        
    })
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    tableView.contentInset.bottom = 50.0
    
    if let footerView = self.commentRelatedView {
      tableView.tableFooterView = footerView
    }
    
    self.addLoadingViewAndAnimating()
  }
  
  // MARK: Method
  func reloadData() {
    shouldLoading = false
    hideLoadingView()
    tableView.text = modelController.contentEn()
    commentRelatedView?.reloadData()
  }
  
  // MARK: IBAction
  
  @IBAction func showMenu(sender: UIButton) {
    FlowManager.presentMenu(from: self)
  }
  
  @IBAction func closeBlogDetail(sender: UIButton) {
    FlowManager.shareInstance.dismissBlogDetailController(from: self)
  }
  
  @IBAction func switchLanguage(sender: UIButton) {
    isEnglish = !isEnglish
    
    let image = isEnglish ? UIImage(named: "learn") : UIImage(named: "not-learn")
    sender.setImage(image, for: .normal)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
}



// MARK: DetailBlogTableViewDelegate
extension BlogDetailViewController: DetailBlogTableViewDelegate {
  func scrollViewDidScrollDown() {
    
    if optionView.alpha == 0.0 { return }
    
    UIView.animate(withDuration: 0.1, animations: {
      self.optionView.alpha = 0.0
      }, completion: { _ in
        //      self.optionView.isHidden = false
    })
  }
  
  func scrollViewDidScrollUp() {
    
    if optionView.alpha == 1.0 { return }
    
    UIView.animate(withDuration: 0.1, animations: {
      self.optionView.alpha = 1.0
      }, completion: { _ in
        //        self.optionView.isHidden = true
    })
  }
  
  func presentPhotoBrowser(viewController: UIViewController) {
    present(viewController, animated: true, completion: nil)
  }
}

// MARK: CommentsRelatedBlogViewDelegate
extension BlogDetailViewController: CommentRelatedBlogViewDelegate {
  func showComments() {
    FlowManager.shareInstance.presentCommentsController(from: self, blogID: self.blogID)
  }
  
  func showRelatedBlog(blog: BlogRelatedModel) {
    FlowManager.shareInstance.presentBlogDetailController(from: self, blogID: blog.id)
  }
}
