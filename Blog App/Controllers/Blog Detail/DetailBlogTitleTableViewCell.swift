//
//  DetailBlogTitleTableViewCell.swift
//  Blog App
//
//  Created by Van Luu on 10/30/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class DetailBlogTitleTableViewCell: UITableViewCell {
  
  // MARK: IBOutlet
  @IBOutlet weak var titleLab: UILabel!
  @IBOutlet weak var dateTimeLab: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = .none
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  func configureTitle(_ text: String) {
    var mutableText = text.replacingOccurrences(of: "<t>", with: "")
    mutableText = mutableText.replacingOccurrences(of: "</t>", with: "|")
    mutableText = mutableText.replacingOccurrences(of: "\n", with: "")
    
    let arrText = mutableText.components(separatedBy: "|")
    if arrText.count > 0 {
      titleLab.text = arrText[0]
      dateTimeLab.text = arrText[1]
    }
  }
  
}
