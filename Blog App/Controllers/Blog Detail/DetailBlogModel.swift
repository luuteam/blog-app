//
//  DetailBlogModel.swift
//  Blog App
//
//  Created by Van Luu on 10/22/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import Foundation
import ObjectMapper

struct BlogRelatedModel {
  var id: String!
  var title: String!
  var image: String!
}

struct DetailBlogModel: ModelProtocol {
  var id: String!
  var commentsCount: Int!
  var series: String!
  var content_en: String!
  var related = [BlogRelatedModel]()

  
  static func parseFromJSON(jsonData: [String : AnyObject]) -> [DetailBlogModel] {
    var arrDetailBlog = [DetailBlogModel]()
    var detailBlog = DetailBlogModel()
    detailBlog.content_en     = jsonData["content-en"] as! String
    detailBlog.series         = jsonData["series"] as! String
    detailBlog.commentsCount  = jsonData["commentsCount"] as! Int
    let relatedBlog           = jsonData["related"] as! [String : AnyObject]
    
    for (key, item) in relatedBlog {
      var related = BlogRelatedModel()
      related.id    = key
      related.image = item["image"] as! String
      related.title = item["title"] as! String
      
      detailBlog.related.append(related)
    }
    
    arrDetailBlog.append(detailBlog)
    
    return arrDetailBlog
  }
}
