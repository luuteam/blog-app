//
//  CommentRelatedBlogCollectionViewFlowLayout.swift
//  Blog App
//
//  Created by Van Luu on 10/15/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class CommentRelatedBlogCollectionViewFlowLayout: UICollectionViewFlowLayout {
  
  private var collectionViewSize: CGSize {
    return collectionView!.bounds.size
  }
  
  private var itemGap: CGFloat {
    return collectionViewSize.width / 14
  }
  
  private var inset: CGFloat {
    return 10.0 + itemGap
  }
  
  func setupLayout() {
    collectionView!.contentInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    minimumLineSpacing = itemGap
    itemSize = CGSize(width: collectionViewSize.width - itemGap*2 - 60, height: collectionViewSize.height)
  }
  
  override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    return true
  }
  
  override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
    var offsetAdjust: CGFloat = 10000
    
    let horizontalCenter = proposedContentOffset.x + collectionView!.bounds.width / 2
    
    let proposedRect = CGRect(x: proposedContentOffset.x,
                              y: 0.0,
                              width: collectionView!.bounds.width,
                              height: collectionView!.bounds.height)
    
    guard let attributesArray = super.layoutAttributesForElements(in: proposedRect) else { return proposedContentOffset }
    
    for attribute in attributesArray {
      if case UICollectionElementCategory.supplementaryView = attribute.representedElementCategory { continue }
      
      let itemHorizontalCenter = attribute.center.x
      if fabs(itemHorizontalCenter - horizontalCenter) < fabs(offsetAdjust) {
        offsetAdjust = itemHorizontalCenter - horizontalCenter
      }
    }
    
    return CGPoint(x: proposedContentOffset.x + offsetAdjust, y: proposedContentOffset.y + offsetAdjust)
  }
}
