//
//  DetailBlogTextTableViewCell.swift
//  Blog App
//
//  Created by Van Luu on 10/14/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class DetailBlogTextTableViewCell: UITableViewCell {
  
  @IBOutlet weak var contentTv: TextView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = .none
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func configureText(text: String, regex: (String, String)) {
    contentTv.regex = regex
    contentTv.paragraph = text
  }
  
}
