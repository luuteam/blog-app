//
//  DetailBlogImageTableViewCell.swift
//  Blog App
//
//  Created by Van Luu on 10/14/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import Kingfisher
import SKPhotoBrowser

class DetailBlogImageTableViewCell: UITableViewCell {
  
  // MARK: IBOutlet
  @IBOutlet weak var imageIma: UIImageView!
  @IBOutlet weak var noteLab: UILabel!
  
  // MARK: Variables
  var browser: SKPhotoBrowser!
  weak var delegate: RelatedImageTableViewCellDelegate!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    imageIma.contentMode = .scaleAspectFill
    noteLab.font = Appearance.fontForSize(size: 13.0)
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    imageIma.image = nil
    noteLab.text = ""
  }
  
  func configureImageNote(text: String) {
    let textWithoutTag = text.replacingOccurrences(of: "<i>", with: "")
    let arrText = textWithoutTag.components(separatedBy: "|")
    
    let resource = URL(string: arrText[0])
    imageIma.kf.setImage(with: resource)
    
    // 4 : have note text | 3 : no note text
    noteLab.text = arrText.count == 4 ? arrText[3] : ""
    
    var images = [SKPhoto]()
    let photo = SKPhoto.photoWithImageURL(arrText[0])
    photo.shouldCachePhotoURLImage = true
    
    images.append(photo)
    browser = SKPhotoBrowser(photos: images)
  }
  
  func showImagePreview() {
    delegate.presentPreviewImaController(controller: browser)
  }
  
}
