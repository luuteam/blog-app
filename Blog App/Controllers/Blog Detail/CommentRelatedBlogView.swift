//
//  CommentRelatedBlogView.swift
//  Blog App
//
//  Created by Van Luu on 10/15/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

protocol CommentRelatedBlogViewDelegate: class {
  func showComments()
  func showRelatedBlog(blog: BlogRelatedModel)
}

class CommentRelatedBlogView: UIView {
  
  // MARK: IBOutlet
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var containerView: UIView!
  weak var modelController: DetailBlogModelController?
  
  weak var delegate: CommentRelatedBlogViewDelegate!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    containerView = Bundle.main.loadNibNamed("CommentRelatedBlogView", owner: self, options: nil)!.first as! UIView
    containerView.frame = self.bounds
    self.addSubview(containerView)
    
    collectionView.layoutIfNeeded()
    configureCollectionView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  
  // MARK: Method
  
  func configureCollectionView() {
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.registerNib(type: BlogRelatedCollectionViewCell.self)
    
    let layoutManager = collectionView.collectionViewLayout as! CommentRelatedBlogCollectionViewFlowLayout
    layoutManager.setupLayout()
  }
  
  func reloadData() {
    collectionView.reloadData()
  }
  
  // MARK: IBAction
  @IBAction func showComments(sender: UIButton) {
    delegate.showComments()
  }

}

// MARK: UICollectionViewDataSource
extension CommentRelatedBlogView: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if modelController == nil { return 0 }
    
    return modelController!.numberOfRelatedBlog()
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(type: BlogRelatedCollectionViewCell.self, forIndexPath: indexPath)!
    
    let relatedBlog = modelController!.relatedBlog(atIndex: indexPath.item)
    cell.configureBlog(imageURL: relatedBlog.image, title: relatedBlog.title)
    
    return cell
  }
}

// MARK: UICollectionViewDelegate
extension CommentRelatedBlogView: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let relatedBlog = modelController!.relatedBlog(atIndex: indexPath.item)
    delegate.showRelatedBlog(blog: relatedBlog)
  }
}
