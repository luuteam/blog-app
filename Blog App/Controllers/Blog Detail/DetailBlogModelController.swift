//
//  DetailBlogModelController.swift
//  Blog App
//
//  Created by Van Luu on 10/22/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import Foundation

class DetailBlogModelController {
  var model: FirebaseModel!
  var currentDetailBlog = DetailBlogModel()
  
  init() {
    model = FirebaseModel(child: "data")
  }
  
  func blogDetail(withBlogID id: String, completionHandler: @escaping (Void) -> Void, failureHandler: (String) -> Void) {
    //model.valueOfItemAtPath(path: id, withSuccess: {}, withFailure: <#T##(((FailureCode, String) -> Void) -> Void)?##(((FailureCode, String) -> Void) -> Void)?##((FailureCode, String) -> Void) -> Void#>)
    model.valueOfItemAtPath(path: id, withSuccess: { (detailBlog : [DetailBlogModel]) in
      
      var detail = detailBlog.first!
      detail.id = id
      self.currentDetailBlog = detail
      
      completionHandler()
      
      }, withFailure: { (msg) in
    
    
    })
  }
  
  func contentEn() -> String {
    return currentDetailBlog.content_en
  }
  
  func numberOfRelatedBlog() -> Int {
    return currentDetailBlog.related.count
  }
  
  func relatedBlog(atIndex index: Int) -> BlogRelatedModel {
    return currentDetailBlog.related[index]
  }
}
