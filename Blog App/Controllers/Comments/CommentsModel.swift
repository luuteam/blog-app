//
//  CommentsModel.swift
//  Blog App
//
//  Created by Van Luu on 10/23/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import ObjectMapper

struct CommentsModel: Mappable, ModelProtocol {
  var id: String!
  var content: String!
  var image: String!
  var username: String!
  var time: TimeInterval!
  var isReply: Bool = false
  var parentID: String? = nil
  var replies: [CommentsModel]?
  
  init?(map: Map) {
    
  }
  
  init() {
    
  }
  
  mutating func mapping(map: Map) {
    content           <- map["content"]
    image             <- map["image"]
    username          <- map["username"]
    time              <- map["time"]
  }
  
  func jsonToWrite() -> Any {
//    return [
//      "content"   : content,
//      "image"     : image,
//      "username"  : username,
//      "time"      : time
//    ]
    
    return [
      "username" : "Luuuuuuu"
    ]
  }
  
  static func parseFromJSON(jsonData: [String : AnyObject]) -> [CommentsModel] {
    var arrComments = [CommentsModel]()
    
    for (id, item) in jsonData {
      let itemData = item as! [String : AnyObject]
      var comment = CommentsModel(JSON: itemData)
      comment?.id = id
      
      if let arrRepliesJSON = item["replies"] as? [String : AnyObject] {
        
        for (idReply, replyItem) in arrRepliesJSON {
          var reply = CommentsModel(JSON: replyItem as! [String : Any])
          reply?.id = idReply
          reply?.isReply = true
          reply?.parentID = id
          
          if comment != nil {
            if comment!.replies == nil {
              comment!.replies = [CommentsModel]()
            }
            
            if let reply = reply {
              comment!.replies!.append(reply)
            }
          }
        }
        
      }
      
      if let _ = comment { arrComments.append(comment!) }
    }
    
    
    return arrComments
  }
}
