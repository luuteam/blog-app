//
//  WriteCommentViewController.swift
//  Blog App
//
//  Created by Van Luu on 10/15/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

protocol WriteCommentViewControllerDelegate: class {
  func didEndWriteComment(_ controller: WriteCommentViewController)
}

class WriteCommentViewController: UIViewController {
  
  // MARK: IBOutlet
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var titleLab: UILabel!
  @IBOutlet weak var contentTv: UITextView!
  @IBOutlet weak var sendBut: UIButton!
  
  // MARK: Variables
  weak var delegate: WriteCommentViewControllerDelegate!
  weak var modelController: CommentsModelController!
  var isReply = false
  var commentReply: CommentsModel? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    contentTv.delegate = self
    contentTv.text = "nhập bình luận của bạn tại đây..."
    contentTv.textColor = UIColor.lightGray
    sendBut.isEnabled = false
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
  // MARK: IBAction
  @IBAction func sendComment(sender: UIButton) {
    guard let content = contentTv.text else { return }
    
    sendBut.isEnabled = false
    modelController.sendComment(commentToReply: commentReply, content: content, withSuccess: {
      
      self.sendBut.isEnabled = true
      self.delegate.didEndWriteComment(self)
      
      }, withFailure: {  msg in
    
    
    })
  }
  
  @IBAction func closeWriteComment(sender: UIButton) {
    FlowManager.exitWriteCommentController(writeCommentVC: self)
  }
  
}

// MARK: WriteCommentViewController: UITextViewDelegate
extension WriteCommentViewController: UITextViewDelegate {
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    if textView.textColor == UIColor.lightGray {
      textView.text = ""
      textView.textColor = UIColor.black
    }
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.text == "" {
      textView.text = "nhập bình luận của bạn tại đây..."
      textView.textColor = UIColor.lightGray
    }
  }
  
  func textViewDidChange(_ textView: UITextView) {
    if textView.text == "" {
      sendBut.setImage(UIImage(named: "send"), for: .normal)
    } else {
      sendBut.setImage(UIImage(named: "send-2"), for: .normal)
    }
    
    if textView.text == "" && textView.textColor == UIColor.black {
      sendBut.isEnabled = false
    } else if textView.textColor == UIColor.lightGray {
      sendBut.isEnabled = false
    } else {
      sendBut.isEnabled = true
    }
  }
}
