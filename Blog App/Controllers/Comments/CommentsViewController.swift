//
//  CommentsViewController.swift
//  Blog App
//
//  Created by Van Luu on 10/15/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class CommentsViewController: BaseViewController, AnimationNavigatorController, NavigationControllerAppearanceContext {
  
  // MARK: IBOutlet
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var noCommentsView: UIView!
  @IBOutlet weak var noCommentsAddCommentBut: UIButton!
  
  // MARK: Variables
  var blogID: String!
  var modelController: CommentsModelController!
  var commentID: String = ""
  var replyButton: ReplyButton!
  var commentReply: CommentsModel? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    modelController = CommentsModelController(blogID: blogID)
    
    tableView.dataSource = self
    tableView.delegate = self
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 150.0
    
    modelController.getMoreComments(withSuccess: {
      
      self.reloadData()
      
      }, withFailure: { msg in
        print(msg)
    })
    
    // reply button
    replyButton = ReplyButton(frame: CGRect(x: 100, y: 100, width: 150.0, height: 50))
    replyButton.isHidden = true
    replyButton.addTarget(self, action: #selector(self.comment), for: .touchUpInside)
    view.addSubview(replyButton)
    
    addGestureToTableView()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    addLoadingViewAndAnimating()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: Method
  func reloadData() {
    shouldLoading = false
    hideLoadingView()
    
    if modelController.numberOfComments() == 0 {
      
      noCommentsView.isHidden = false
      
    } else {
      noCommentsView.isHidden = true
      tableView.reloadData()
    }
  }
  
  func comment() {
    if UserManager.isUserLoggedIn {
      FlowManager.presentWriteComment(from: self, modelController: modelController, comment: commentReply)
    } else {
      FlowManager.shareInstance.setRootVC(userLoggedIn: false)
    }
    
    commentReply = nil
    hideReplyButton()
    tableView.isScrollEnabled = true
  }
  
  func addGestureToTableView() {
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handlerTapInTableView(gesture:)))
    tableView.addGestureRecognizer(tapGesture)
  }
  
  func handlerTapInTableView(gesture: UITapGestureRecognizer) {
    let location = gesture.location(in: view)
    let locationInTableView = tableView.convert(location, from: view)
    
    guard let indexPath = tableView.indexPathForRow(at: locationInTableView) else {
      return
    }
    
    if replyButton.isHidden {
      showReplyButton(at: location)
      tableView.isScrollEnabled = false
    } else {
      tableView.isScrollEnabled = true
      hideReplyButton()
      commentReply = nil
      
      return
    }
    
    switch modelController.comment(atIndex: indexPath.row) {
    case CommentCellType.comment(let comment):
      commentReply = comment
    case CommentCellType.reply(let reply):
      commentReply = reply
    default:
      break
    }
  }
  
  func showReplyButton(at point: CGPoint) {
    var origin: CGPoint!
    if point.x + replyButton.frame.width > view.frame.width - 10 {
      origin = CGPoint(x: point.x - replyButton.frame.width, y: point.y - 50.0)
    } else {
      origin = CGPoint(x: point.x, y: point.y - 50.0)
    }
    
    replyButton.frame.origin = origin
    showHideReplyButton(show: true)
  }
  
  func hideReplyButton() {
    showHideReplyButton(show: false)
  }
  
  func showHideReplyButton(show: Bool) {
    if show {
      replyButton.isHidden = false
      replyButton.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
      UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: .curveEaseInOut, animations: {
        
        self.replyButton.transform = CGAffineTransform.identity
        }, completion: nil)
      
    } else {
      
      UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: .curveEaseInOut, animations: {
        
        self.replyButton.transform = CGAffineTransform.identity
        }, completion: { _ in
        self.replyButton.isHidden = true
        self.replyButton.transform = CGAffineTransform.identity
      })
    }
  }
  
  // MARK: IBAction
  @IBAction func closeComments(sender: UIButton) {
    FlowManager.shareInstance.dismissCommentController(from: self)
  }
  
  @IBAction func writeNewComment(sender: UIButton) {
    comment()
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
}

// MARK: UITableViewDataSource
extension CommentsViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return modelController.numberOfComments()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let commentType = modelController.comment(atIndex: indexPath.row)
    
    switch commentType {
    case CommentCellType.comment(let comment):
      let cell = tableView.dequeueReusableCell(type: CommentTableViewCell.self)!
      cell.configureComment(comment: comment)
      
      return cell
      
    case CommentCellType.reply(let comment):
      let cell = tableView.dequeueReusableCell(type: ReplyCommentTableViewCell.self)!
      cell.configureComment(comment: comment)
      
      return cell
      
    case CommentCellType.loadMore:
      let cell = tableView.dequeueReusableCell(type: LoadMoreTableViewCell.self)!
      cell.startAnimate()
      
      modelController.getMoreComments(withSuccess: {
        
        self.reloadData()
        
        }, withFailure: { msg in
          print(msg)
      })
      
      return cell
    }
  }
}

// MARK: UITableViewDelegate
extension CommentsViewController: UITableViewDelegate {

}

// MARK: WriteCommentViewControllerDelegate
extension CommentsViewController: WriteCommentViewControllerDelegate {
  func didEndWriteComment(_ controller: WriteCommentViewController) {
    FlowManager.exitWriteCommentController(writeCommentVC: controller)
    reloadData()
  }
}
