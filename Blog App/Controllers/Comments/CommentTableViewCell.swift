//
//  CommentTableViewCell.swift
//  Blog App
//
//  Created by Van Luu on 10/15/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import Kingfisher

class CommentTableViewCell: UITableViewCell {
  
  // MARK: IBOutlet
  @IBOutlet weak var userIma: UIImageView!
  @IBOutlet weak var usernameLab: UILabel!
  @IBOutlet weak var contentLab: UILabel!
  @IBOutlet weak var timeLab: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    let selectedBackgroundView = UIView(frame: self.bounds)
    selectedBackgroundView.backgroundColor = AppColor.commentIma
    self.selectedBackgroundView = selectedBackgroundView
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  // MARK: Method
  func configureComment(comment: CommentsModel) {
    let resource = URL(string: comment.image)
    userIma.kf.setImage(with: resource)
    
    usernameLab.text = comment.username
    contentLab.text = comment.content
    timeLab.text = comment.time.timeFormat(formatter: Formatter.commentFormatter)
  }
  
}
