//
//  CommensModelController.swift
//  Blog App
//
//  Created by Van Luu on 10/22/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

enum CommentCellType {
  case comment(CommentsModel)
  case reply(CommentsModel)
  case loadMore
}

class CommentsModelController {
  var model: FirebaseModel!
  var lastTimeInterval: TimeInterval = 0.0
  var currentComments: [CommentCellType]!
  var shouldGetMoreComments = false
  var currentCommentCount = 0
  
  init(blogID: String) {
    model = FirebaseModel(child: "comments/\(blogID)")
    currentComments = [CommentCellType]()
  }
  
  func reloadComments(withSuccess: @escaping (Void) -> Void, withFailure: (String) -> Void) {
    
    var condition = Condition()
    condition.sortByChild = "time"
    condition.queryParam = (key: "time", value: lastTimeInterval)
    condition.isStarting = false
    
    model.itemsWithCondition(condition: condition, withSuccess: { [weak self] (listComments : [CommentsModel]) in
      
      guard let this = self else { return }
      this.processReturnComments(listComments: listComments, isReload: true)
      
      withSuccess()
      
      }, withFailure: { (msg) in

    })
  }
  
  func getMoreComments(withSuccess: @escaping (Void) -> Void, withFailure: (String) -> Void) {
    
    var condition = Condition()
    condition.sortByChild = "time"
    condition.queryParam = (key: "time", value: lastTimeInterval)
    condition.isStarting = true
    condition.limitAndtoFirst = (limit: UInt(5), isToFirst: true)
    
    model.itemsWithCondition(condition: condition, withSuccess: { [weak self] (listComments : [CommentsModel]) in
      
      guard let this = self else { return }
      this.processReturnComments(listComments: listComments)
      
      withSuccess()
      
      }, withFailure: { (msg) in
        
    })
  }
  
  func sendComment(commentToReply: CommentsModel?, content: String, withSuccess: @escaping (Void) -> Void, withFailure: (String) -> Void) {
    var comment = CommentsModel()
    comment.content = content
    comment.time = NSDate().timeIntervalSince1970
    comment.username = UserManager.username
    comment.image = UserManager.userAvatar?.absoluteString ?? "https://firebasestorage.googleapis.com/v0/b/blogapp-b69c0.appspot.com/o/unknown-256.png?alt=media&token=a2459e76-164b-425c-92b7-5839ffd71832"
    
      var json = [
          "content"   : comment.content,
          "image"     : comment.image,
          "username"  : comment.username,
          "time"      : comment.time
        ] as [String : Any]
    
    lastTimeInterval = comment.time + 0.1
    
    var path = ""
    var parentCommentID = ""
    
    if let parent = commentToReply, parent.isReply { // reply a reply
      let content = "@\(parent.username!): " + "\(json["content"]!)"
      json["content"] = content
      
      parentCommentID = parent.parentID ?? ""
      
    } else if let parent = commentToReply { // reply a comment
      parentCommentID = parent.id
      
    } else { // write a comment
      parentCommentID = ""
    }
    
    if parentCommentID != "" {
      path = parentCommentID + "/" + "replies"
    }
    
    model.writeOneData(atPath: path, autoID: true, content: json, withSuccess: {
      
      self.reloadComments(withSuccess: {
        
        withSuccess()
        
        }, withFailure: { msg in
      
      
      })
      
      }, withFailure: { (msg) in
    
      
    })
  }
  
  private func processReturnComments(listComments: [CommentsModel], isReload: Bool = false) {

    currentCommentCount = listComments.count
    
    let sortedComments = listComments.sorted {$0.time < $1.time}
    if let lastTime = sortedComments.last?.time {
      lastTimeInterval = lastTime
    }
    
    if sortedComments.count % 5 == 0 && sortedComments.count > 0 {
      shouldGetMoreComments = true
    } else {
      shouldGetMoreComments = false
    }
    
    if isReload {
      currentComments.removeAll()
    }
    
    if currentComments.count > 0 {
      if case CommentCellType.loadMore(_) = currentComments.last! {
        currentComments.remove(at: currentComments.count - 1)
      }
    }
    
    for comment in sortedComments {
      currentComments.append(CommentCellType.comment(comment))
      if comment.replies != nil {
        let sortedReplies = comment.replies!.sorted {$0.time < $1.time}
        currentComments.append(contentsOf: sortedReplies.map {
          return CommentCellType.reply($0)
        })
      }
    }
    
    if shouldGetMoreComments {
      currentComments.append(CommentCellType.loadMore)
    }
  }
  
  func getRepliesComment(atIndex index: Int, withSuccess: @escaping (Void) -> Void, withFailure: (String) -> Void) {
    if case CommentCellType.comment(let comment) = currentComments[index - 1] {
      if let replies = comment.replies {
        
        currentComments.remove(at: index) // remove load more reply cell
        
        for (i, item) in replies.enumerated() {
          currentComments.insert(CommentCellType.reply(item), at: index + i)
        }
        
        withSuccess()
        return
      }
      
      withFailure("No Replies With Comment")
    }
    
    withFailure("No Comment Exist")
  }
  
  func numberOfComments() -> Int {
    return currentComments.count
  }
  
  func comment(atIndex index: Int) -> CommentCellType {
    return currentComments[index]
  }
  
  
}
