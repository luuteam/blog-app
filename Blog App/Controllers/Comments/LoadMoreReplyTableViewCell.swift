//
//  LoadMoreReplyTableViewCell.swift
//  Blog App
//
//  Created by Van Luu on 10/23/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class LoadMoreReplyTableViewCell: UITableViewCell {

  // MARK: IBOutlet
  @IBOutlet weak var loadMoreBut: UIButton!
  
  // MARK: Variables
  var actionHandler: ((Void) -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
    actionHandler = nil
    loadMoreBut.setTitleColor(AppColor.menuSearch, for: .normal)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    print(self.frame)
  }
  
  // MARK: Method
  func configureLoadMore(loadMoreText text: String, actionHandler: @escaping (Void) -> Void) {
    loadMoreBut.setTitle(text, for: .normal)
    self.actionHandler = actionHandler
  }
  
  // MARK: IBAction
  @IBAction func loadMore(sender: UIButton) {
    loadMoreBut.setTitleColor(UIColor.darkGray, for: .normal)
    actionHandler?()
  }

}
