//
//  LoadMoreTableViewCell.swift
//  Blog App
//
//  Created by Van Luu on 10/23/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class LoadMoreTableViewCell: UITableViewCell {
  
  // MARK: IBOutlet
  @IBOutlet weak var loadMoreView: LoadMoreView!
  
  // MARK: Variables
  var actionHandler: ((Void) -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
    actionHandler = nil
    loadMoreView.stopAnimate()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    //
    loadMoreView.frame = self.bounds
  }
  
  // MARK: Method
  func startAnimate() {
    loadMoreView.animate()
  }
  
}
