//
//  BaseViewController.swift
//  Blog App
//
//  Created by Van Luu on 10/23/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
  
  var shouldLoading = true
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: Method
  func addLoadingViewAndAnimating() {
    
    if !shouldLoading { return }
    
    if let _ = view.viewWithTag(Tag.LoadingView.view) { return }
    
    let loadingView = UIView()
    loadingView.frame = view.bounds
    loadingView.tag = Tag.LoadingView.view
    loadingView.backgroundColor = UIColor.white
    view.addSubview(loadingView)
    view.bringSubview(toFront: loadingView)
    
    let loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    loadingIndicator.center = loadingView.center
    loadingIndicator.frame.size = CGSize(width: 50, height: 50)
    loadingIndicator.color = AppColor.menuSearch
    loadingIndicator.startAnimating()
    
    let backButton = UIButton()
    backButton.frame = CGRect(x: 16.0, y: 16.0, width: 32.0, height: 32.0)
    backButton.setImage(UIImage(named: "back"), for: .normal)
    backButton.addTarget(self, action: #selector(BaseViewController.exitCurrentScreen), for: .touchUpInside)
    
    loadingView.addSubview(loadingIndicator)
    loadingView.addSubview(backButton)
  }
  
  func hideLoadingView() {
    guard let loadingView = view.viewWithTag(Tag.LoadingView.view) else { return }
    UIView.animate(withDuration: 0.2, animations: {
      
      loadingView.alpha = 0.0
      
      }, completion: { _ in
        
        loadingView.removeFromSuperview()
        
    })
  }
  
  func exitCurrentScreen() {
    FlowManager.shareInstance.exitCurrentVC()
  }
  
}
