//
//  SearchItemTableViewCell.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import Kingfisher

class SearchItemTableViewCell: UITableViewCell {
  
  @IBOutlet weak var blogIma: UIImageView!
  @IBOutlet weak var titleLab: UILabel!
  @IBOutlet weak var descriptionLab: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  func configure(title: String, description: String, imageURL: String) {
    let resource = URL(string: imageURL)
    blogIma.kf.setImage(with: resource)
    
    titleLab.text = title
    descriptionLab.text = description
  }
  
}
