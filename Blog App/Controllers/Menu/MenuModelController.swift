//
//  MenuModelController.swift
//  Blog App
//
//  Created by Van Luu on 11/2/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class MenuModelController {
  var model: FirebaseModel!
  var currentSearchItem: [BlogItemModel]!
  
  init() {
    model = FirebaseModel(child: "list")
    currentSearchItem = [BlogItemModel]()
  }
  
  func getListBlogItemWithTitle(title: String, completionHandler: @escaping (Void) -> Void, failureHandler: (String) -> Void) {
    let capitalizeTitle = (title as NSString).capitalized
    var condition = Condition()
    condition.queryParam = (key: "title", value: capitalizeTitle)
    condition.isStarting = true
    condition.sortByChild = "title"
    
    model.itemsWithCondition(condition: condition, withSuccess: { (listItem : [BlogItemModel]) in
      self.currentSearchItem = listItem.filter { item in
        if String.contains(item.title, substring: capitalizeTitle) { return true }
        return false
      }
      
      completionHandler()
      
      }, withFailure: { (msg) in
        
        print(msg)
    })
  }
  
  func numberOfItem() -> Int {
    return currentSearchItem.count
  }
  
  func item(atIndex index: Int) -> BlogItemModel {
    return currentSearchItem[index]
  }

}
