//
//  SearchResultProvider.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class SearchResultProvider: NSObject {
  
  weak var tableView: UITableView!
  
  init(tableView: UITableView) {
    super.init()
    self.tableView = tableView
    registerCell()
  }
  
  private func registerCell() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.registerNib(type: SearchItemTableViewCell.self)
  }
}

// MARK: UITableViewDataSource
extension SearchResultProvider: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 10
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return tableView.dequeueReusableCell(type: SearchItemTableViewCell.self)!
  }
  
}

// MARK: UITableViewDelegate
extension SearchResultProvider: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }
}
