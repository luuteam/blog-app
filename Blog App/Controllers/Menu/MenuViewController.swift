//
//  MenuViewController.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
  
  // MARK: IBOutlet
  @IBOutlet weak var menuView: UIView!
  @IBOutlet weak var searchBut: UIButton!
  @IBOutlet weak var cancelSearchBtn: UIButton!
  @IBOutlet weak var exitBtn: UIButton!
  @IBOutlet weak var searchStackView: UIStackView!
  @IBOutlet weak var searchTf: UITextField!
  @IBOutlet weak var searchTableView: UITableView!
  
  // MARK: Variables
  var isSearching = false
  var modelController: MenuModelController!
  var previousSearchTitle = ""
  var isAnimated = false
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    modelController = MenuModelController()
    
    searchTableView.dataSource = self
    searchTableView.delegate = self
    searchTableView.registerNib(type: SearchItemTableViewCell.self)
    searchTableView.registerNib(type: NoSearchDataTableViewCell.self)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
//    if isAnimated { return }
    
//    isAnimated = true
    animateMenu()
  }
  
  // MARK: Methods
  private func animateMenu() {
    var delay = 0.0
    
    // animate Menu Stack View
    for i in Tag.Menu.home...Tag.Menu.logIn {
      let button = menuView.viewWithTag(i)
      let animation = moveHorizontalAnimation(withDelay: delay)
      animation.delegate = self
      animation.setValue(button?.layer, forKey: "layer")
      button?.layer.add(animation, forKey: nil)
      
      delay += AnimationDuration.menu/2
    }
    
    delay = 0.0
    
    // animate Search Button
    let animation = moveHorizontalAnimation(withDelay: delay)
    animation.delegate = self
    animation.setValue(searchBut.layer, forKey: "layer")
    searchBut.layer.add(animation, forKey: nil)
  }
  
  private func moveHorizontalAnimation(withDelay delay: CFTimeInterval, fromRight: Bool = true) -> CABasicAnimation {
    let animation = CABasicAnimation(keyPath: "transform.translation.x")
    animation.fromValue   = fromRight ? 150 : 0
    animation.toValue     = fromRight ? 0 : 150
    animation.duration    = AnimationDuration.menu
    animation.beginTime   = CACurrentMediaTime() + delay
    animation.fillMode    = kCAFillModeBackwards
    
    return animation
  }
  
  private func moveVerticalAnimation(withDelay delay: CFTimeInterval, fromUp: Bool = true) -> CABasicAnimation {
    let animation = CABasicAnimation(keyPath: "transform.translation.y")
    animation.fromValue   = fromUp ? -150 : 0
    animation.toValue     = fromUp ? 0 : -150
    animation.duration    = AnimationDuration.menu
    animation.fillMode    = kCAFillModeBackwards
    animation.beginTime   = CACurrentMediaTime() + delay
    
    return animation
  }
  
  private func animateSearchingAndCancel(shouldDoSearching searching: Bool) {
    
    UIView.animate(withDuration: 0.2) {
      if searching {
        self.menuView.transform = CGAffineTransform(translationX: 150, y: 0)
      } else {
        self.menuView.transform = self.menuView.transform.translatedBy(x: -150, y: 0)
      }
    }
    
    searchBut.isHidden = searching ? true : false
    cancelSearchBtn.isHidden = searching ? false : true
    exitBtn.isHidden = searching ? true : false
    searchTableView.isHidden = searching ? false : true
    searchStackView.isHidden = searching ? false : true
  }
  
  // MARK: IBAction
  @IBAction func exit(sender: UIButton) {
    FlowManager.exitMenu(menuVC: self)
  }
  
  @IBAction func search(sender: UIButton) {
    searchTf.becomeFirstResponder()
    isSearching = true
    animateSearchingAndCancel(shouldDoSearching: isSearching)
  }
  
  @IBAction func cancelSearch(sender: UIButton) {
    isSearching = false
    view.endEditing(true)
    animateSearchingAndCancel(shouldDoSearching: isSearching)
  }
  
  @IBAction func logOut(sender: UIButton) {
    UserManager.logOut { msg in
      
      if let msg = msg {
        print(msg)
      } else {
        FlowManager.shareInstance.setRootVC(userLoggedIn: false)
      }
    }
  }
  
  @IBAction func backToHome(sender: UIButton) {
    FlowManager.shareInstance.popToRoot()
    FlowManager.exitMenu(menuVC: self)
  }
  
  @IBAction func executeSearch(sender: UIButton) {
    let title = self.searchTf.text ?? ""
    
    if title == "" {
      return
    }
    
    if title == previousSearchTitle {
      return
    }
    
    previousSearchTitle = title
    
    view.endEditing(true)
    animateTableView(show: false) {
      
      self.modelController.getListBlogItemWithTitle(title: title, completionHandler: { [weak self] in
        guard let this = self else { return }
        
        this.reloadData()
        
        }, failureHandler: { msg in
          
          
      })
      
    }
  }
  
  @IBAction func aboutMe(sender: UIButton) {
    FlowManager.shareInstance.presentAboutUs()
  }
  
  // MARK: Method
  func reloadData() {
    searchTableView.reloadData()
    animateTableView(show: true)
    
  }
  
  func animateTableView(show: Bool, completion: ((Void) -> Void)? = nil) {
    
    let visibleCells = searchTableView.visibleCells

    if !show {
      
      if visibleCells.count == 0 {
        searchTableView.isHidden = true
        completion?()
        return
      }
      
      for (index, cell) in visibleCells.enumerated() {
        UIView.animate(withDuration: 0.4 + (0.1 * Double(index)), animations: {
          cell.transform = CGAffineTransform(translationX: -500, y: 0)
          
          }, completion: { _ in
            if index == visibleCells.count - 1{
              self.searchTableView.isHidden = true
              
              // reset transform
              for cell2 in visibleCells {
                cell2.transform = CGAffineTransform.identity
              }
              
              completion?()
            }
            
        })
      }
      
    } else {
      DispatchQueue.main.async {
        for cell in visibleCells {
          cell.transform = CGAffineTransform(translationX: 500, y: 0)
        }
        
        self.searchTableView.isHidden = false
        
        for (index,cell) in visibleCells.enumerated() {
          UIView.animate(withDuration: 0.4 + (0.1 * Double(index)), animations: {
            cell.transform = CGAffineTransform(translationX: 0, y: 0)
          })
        }
      }
      
      
    }
  }
}

// MARK: UITableViewDataSource
extension MenuViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if modelController.numberOfItem() > 0 {
      return modelController.numberOfItem()
    }
    
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if modelController.numberOfItem() == 0 {
      return tableView.dequeueReusableCell(type: NoSearchDataTableViewCell.self)!
    }
    
    let cell = tableView.dequeueReusableCell(type: SearchItemTableViewCell.self)!
    let blog = modelController.item(atIndex: indexPath.item)
    
    cell.configure(title: blog.title, description: blog.description, imageURL: blog.image)
    
    return cell
  }
}

// MARK: UITableViewDelegate
extension MenuViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    
    if modelController.numberOfItem() > 0 {
      
      let blog = modelController.item(atIndex: indexPath.item)
      FlowManager.shareInstance.presentBlogDetailController(from: self.parent!, blogID: blog.id)
    }
  }
}

// MARK: CABasicAnimationDelegate
extension MenuViewController: CAAnimationDelegate {
  func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
    if let layer = anim.value(forKey: "layer") as? CALayer {
      layer.removeAllAnimations()
    }
  }
}
