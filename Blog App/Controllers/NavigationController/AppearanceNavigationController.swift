//
//  AppearanceNavigationController.swift
//  Blog App
//
//  Created by Van Luu on 10/15/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class AppearanceNavigationController: UINavigationController {
  
  required init?(coder decoder: NSCoder) {
    super.init(coder: decoder)
    
    interactivePopGestureRecognizer?.isEnabled = false
//    delegate = self
  }
  
  override init(rootViewController: UIViewController) {
    super.init(rootViewController: rootViewController)
    
    interactivePopGestureRecognizer?.isEnabled = false
//    delegate = self
  }
  
  // MARK: - UINavigationControllerDelegate
  
  
  // MARK: - Appearance Applying
  private var appliedAppearance: Appearance?
  
  private func applyAppearance(appearance: Appearance?, navigationItem: UINavigationItem?, animated: Bool) {
    if let appearance = appearance {
      appliedAppearance = appearance
      
      //      appearanceApplyingStrategy.apply(appearance, toNavigationController: self, navigationItem:  navigationItem, animated: animated)
      setNeedsStatusBarAppearanceUpdate()
    }
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
}
