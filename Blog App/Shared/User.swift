//
//  User.swift
//  Blog App
//
//  Created by Van Luu on 10/18/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import Foundation

class User {
  
  static var userDefault = UserDefaults.standard
  
  static func save(user: UserInfo) {
    userDefault.set(user.toDictionary(), forKey: UserKey.userInformation)
  }
}
