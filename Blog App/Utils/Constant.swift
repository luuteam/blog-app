//
//  Constant.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

func delay(second: Int, handler: @escaping (() -> ())) {
  let deadlineTime = DispatchTime.now() + .seconds(second)
  DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
    handler()
  }
}

struct Identifier {
  static let blogListItemsCell = "BlogListItemsCell"
  static let searchItemCell    = "SearchItemCell"
  static let detailBlogCell    = "DetailBlogCell"
}

struct RegularRegex {
  static let image            = "<i>([^#]+)<i>"
  static let title            = "<t>([^#]+)<t>"
  static let relatedImage     = "<r>([^#]+)<r>"
}

struct AppColor {
  static let mainItemColor = UIColor.colorWithRedValue(redValue: 255, greenValue: 255, blueValue: 157, alpha: 1.0)
  static let menuColor     = UIColor.colorWithRedValue(redValue: 255, greenValue: 255, blueValue: 250, alpha: 1.0)
  static let menuSearch    = UIColor(netHex: 0xE88256)
  static let menuText      = UIColor(netHex: 0x5E6A73)
  static let commentIma    = UIColor(netHex: 0xD8DADA)
  static let title         = UIColor(red: 0, green: 0, blue: 0)
  static let time          = UIColor.gray
  static let blogContent   = UIColor.darkGray
  static let hourGlass     = UIColor.black
  static let green         = UIColor(red: 0, green: 128, blue: 64)
}

struct Tag {
  struct Menu {
    static let home       = 11
    static let about      = 12
    static let logIn      = 13
    static let logOut     = 14
    static let facebook   = 15
    static let twitter    = 16
    static let search     = 17
    static let cancel     = 18
  }
  struct LoadingView {
    static let view       = 123
  }
}

struct AnimationDuration {
  static let menu = 0.2
}

struct UserKey {
  static var userInformation = "UserInformation"
  static var email = "email"
  static var displayName = "displayName"
  static var uID = "uID"
}

struct Formatter {
  static let commentFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd-MM-yyyy"
    return formatter
  }()
}

func aspectFillSize(imageSize: CGSize, withWidth width: CGFloat) -> CGSize {
  return CGSize.zero
}


