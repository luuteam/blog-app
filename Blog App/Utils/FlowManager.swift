//
//  FlowManager.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

class FlowManager: NSObject {
  
  static let shareInstance = FlowManager()
  weak var navigationController: UINavigationController! {
    didSet {
      navigationController.delegate = self
    }
  }
  
  enum flowCase {
    case none
    case presentDetailBlog
    case presentComment
    case dismissComment
    case dismissDetailBlog
    case presentListItem
    case dismissListItem
  }
  
  var flow = flowCase.none
  
  let scaleTransition = ScaleUpAndDownTransition()
  lazy var scaleAlphaTransition = ScaleAlphaTransition()
  
  private static var menuVC: MenuViewController = {
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let vcB = storyBoard.instantiateViewController(type: MenuViewController.self)!
    
    return vcB
  }()
  
  private static var sectionVC: SectionsViewController = {
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let vcB = storyBoard.instantiateViewController(type: SectionsViewController.self)!
    
    return vcB
  }()
  
  private override init() {}
  
  func setRootVC(userLoggedIn: Bool) {
    
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    
    if userLoggedIn {
      let listItemVC = storyBoard.instantiateViewController(type: BlogListItemsViewController.self)!
      navigationController.viewControllers = [listItemVC]
    } else {
      let loginSignUpVC = storyBoard.instantiateViewController(type: LoginSignUpViewController.self)!
      navigationController.viewControllers = [loginSignUpVC]
    }
  }
  
  static func presentMenu(from vcA: UIViewController) {
    if menuVC.parent != nil {
      menuVC.willMove(toParentViewController: nil)
      menuVC.view.removeFromSuperview()
      menuVC.removeFromParentViewController()
    }
    
    vcA.addChildViewController(menuVC)
    menuVC.view.frame = vcA.view.bounds
    vcA.view.addSubview(menuVC.view)
    menuVC.didMove(toParentViewController: vcA)
    
    menuVC.view.alpha = 0.0
    UIView.animate(withDuration: 0.2) {
      menuVC.view.alpha = 1.0
    }
  }
  
  static func exitMenu(menuVC: MenuViewController) {
    UIView.animate(withDuration: 0.2, animations: {
      menuVC.view.alpha = 0.0
      
      }, completion: { _ in
        menuVC.willMove(toParentViewController: nil)
        menuVC.view.removeFromSuperview()
        menuVC.removeFromParentViewController()
    })
  }
  
  static func presentSection(from vcA: BlogListItemsViewController) {
    if sectionVC.parent != nil {
      sectionVC.willMove(toParentViewController: nil)
      sectionVC.view.removeFromSuperview()
      sectionVC.removeFromParentViewController()
    }
    
    vcA.addChildViewController(sectionVC)
    sectionVC.view.frame = vcA.view.bounds
    vcA.view.addSubview(sectionVC.view)
    sectionVC.didMove(toParentViewController: vcA)
    
    sectionVC.view.alpha = 0.0
    UIView.animate(withDuration: 0.2) {
      sectionVC.view.alpha = 1.0
    }
  }
  
  static func exitSection(menuVC: SectionsViewController) {
    UIView.animate(withDuration: 0.2, animations: {
      menuVC.view.alpha = 0.0
      
    }, completion: { _ in
      menuVC.willMove(toParentViewController: nil)
      menuVC.view.removeFromSuperview()
      menuVC.removeFromParentViewController()
    })
  }
  
  // MARK: Login Sign Up
  
  static func presentLogin(from vcA: UIViewController) {
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let vcB = storyBoard.instantiateViewController(type: LoginViewController.self)!
    
    vcA.addChildViewController(vcB)
    vcB.view.frame = vcA.view.bounds
    
//    vcB.view.alpha = 0.0
    
    vcA.view.addSubview(vcB.view)
    vcB.didMove(toParentViewController: vcA)
    
//    UIView.animate(withDuration: 0.4) {
//      vcB.view.alpha = 1.0
//    }
  }
  
  static func presentSignUp(from vcA: UIViewController) {
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let vcB = storyBoard.instantiateViewController(type: SignUpViewController.self)!
    
    vcA.addChildViewController(vcB)
    vcB.view.frame = vcA.view.bounds
    vcA.view.addSubview(vcB.view)
    vcB.didMove(toParentViewController: vcA)
    
//    vcB.view.alpha = 0.0
//    UIView.animate(withDuration: 0.4) {
//      vcB.view.alpha = 1.0
//    }
  }
  
  static func exitLoginSignUp(menuVC: UIViewController) {
    UIView.animate(withDuration: 0.2, animations: {
      menuVC.view.alpha = 0.0
      
      }, completion: { _ in
        menuVC.willMove(toParentViewController: nil)
        menuVC.view.removeFromSuperview()
        menuVC.removeFromParentViewController()
    })
  }
  
  
  // MARK: CommentViewController
  func presentCommentsController(from vcA: UIViewController, blogID: String) {
    flow = flowCase.presentComment
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let vcB = storyBoard.instantiateViewController(type: CommentsViewController.self)!
    vcB.blogID = blogID
    
    self.navigationController.pushViewController(vcB, animated: true)
  }
  
  func dismissCommentController(from vcA: UIViewController) {
    flow = flowCase.dismissComment
    self.navigationController.popViewController(animated: true)
  }
  
  // MARK: BlogDetailViewController
  func presentBlogDetailController(from vcA: UIViewController, blogID: String) {
    flow = flowCase.presentDetailBlog
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let vcB = storyBoard.instantiateViewController(type: BlogDetailViewController.self)!
    vcB.blogID = blogID
    
    self.navigationController.pushViewController(vcB, animated: true)
  }
  
  func dismissBlogDetailController(from vcA: UIViewController) {
    flow = flowCase.dismissDetailBlog
    vcA.navigationController!.popViewController(animated: true)
  }
  
  // MARK: BlogListItemViewController
  func presentBlogListItemController(from vcA: UIViewController) {
    flow = flowCase.presentListItem
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let vcB = storyBoard.instantiateViewController(type: BlogListItemsViewController.self)!
    
    self.navigationController.pushViewController(vcB, animated: true)
  }
  
  // MARK: Comment
  
  static func presentWriteComment(from vcA: CommentsViewController, modelController: CommentsModelController, comment: CommentsModel? = nil) {
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let vcB = storyBoard.instantiateViewController(type: WriteCommentViewController.self)!
    vcB.modelController = modelController
    vcB.delegate = vcA
    
    if comment != nil { vcB.commentReply = comment! }
    
    vcA.addChildViewController(vcB)
    vcB.view.frame = vcA.view.bounds
    vcA.view.addSubview(vcB.view)
    vcB.didMove(toParentViewController: vcA)
    
    vcB.view.alpha = 0.0
    UIView.animate(withDuration: 0.2) {
      vcB.view.alpha = 1.0
    }
  }
  
  static func exitWriteCommentController(writeCommentVC vc: WriteCommentViewController) {
    UIView.animate(withDuration: 0.2, animations: {
      vc.view.alpha = 0.0
      
      }, completion: { _ in
        vc.willMove(toParentViewController: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParentViewController()
    })
  }
  
  // MARK: Image Chooser
  static func presentImageChoose(from vcA: SignUpViewController) {
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let vcB = storyBoard.instantiateViewController(type: ImageChooseViewController.self)!
    
    vcB.delegate = vcA
    
    vcA.present(vcB, animated: true, completion: nil)
  }
  
  // MARK: Other
  static func dismissViewController(from vcA: UIViewController) {
    vcA.dismiss(animated: true, completion: nil)
  }
  
  func exitCurrentVC() {
    navigationController.popViewController(animated: true)
  }
  
  func popToRoot() {
    navigationController.popToRootViewController(animated: true)
  }
  
  // MARK: AboutUsViewController
  func presentAboutUs() {
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let vcB = storyBoard.instantiateViewController(type: AboutUsViewController.self)!
    
    navigationController.pushViewController(vcB, animated: true)
  }
  
}

// MARK: UINavigationControllerDelegate
extension FlowManager: UINavigationControllerDelegate {
  func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    switch self.flow {
    case .presentComment, .dismissComment:
      scaleTransition.operation = operation
      return scaleTransition
    case .presentDetailBlog:
      scaleAlphaTransition.operation = operation
      return scaleAlphaTransition
    case .dismissDetailBlog:
      scaleTransition.operation = operation
      return scaleTransition
    case .presentListItem:
      scaleTransition.operation = operation
      return scaleTransition
    default:
      return nil
    }
  }
  
  func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    
    guard let appearanceContext = viewController as? NavigationControllerAppearanceContext else {
      return
    }
    navigationController.setNavigationBarHidden(appearanceContext.prefersNavigationControllerBarHidden(navigationController: navigationController), animated: animated)
    navigationController.setToolbarHidden(appearanceContext.prefersNavigationControllerToolbarHidden(navigationController: navigationController), animated: animated)
    
  }
}
