//
//  UserManager.swift
//  Blog App
//
//  Created by Van Luu on 10/29/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import Foundation
import Firebase

class UserManager {
  
  static var username = ""
  static var userAvatar: URL? = nil
  static var isUserLoggedIn = false
  
  private static var authHandler: FIRAuthStateDidChangeListenerHandle? = nil
  
  static func listenUserState(handler: ((Bool) -> Void)?) {
  
    authHandler = FIRAuth.auth()?.addStateDidChangeListener { (auth, user) in

      if let user = user {
        self.setUserInformation(isLogged: true, name: user.displayName ?? "default", avatar: user.photoURL)
        
      } else {
        
        self.setUserInformation(isLogged: false, name: "", avatar: nil)
      }
      
      if let handler = handler {
        handler(self.isUserLoggedIn)
      }
    }
  }
  
  static func registerUser(userInfo: UserLoginSignUp, handler: @escaping (String?) -> Void) {
    FIRAuth.auth()?.createUser(withEmail: userInfo.email, password: userInfo.password, completion: { (user, error) in
      
      if let user = user {
        let changeRequest = user.profileChangeRequest()
        changeRequest.displayName = userInfo.userName
        changeRequest.photoURL = URL(string: userInfo.userAvatar)
        
        changeRequest.commitChanges { error in
          
          self.setUserInformation(isLogged: true, name: userInfo.userName, avatar: URL(string: userInfo.userAvatar))
          handler(error?.localizedDescription)
        }
        
      } else {
        handler(error?.localizedDescription)
      }
    
    })
  }
  
  static func loginUser(email: String, password: String, handler: @escaping (String?) -> Void) {
    FIRAuth.auth()?.signIn(withEmail: email, password: password, completion: { (user, error) in
    
      if error == nil {
        handler(nil)
      } else {
        handler(getReadableErrorString(error: error!))
        
      }
    })
  }
  
  static func logOut(handler: (String?) -> Void) {
    do {
      
      try FIRAuth.auth()?.signOut()
      
      handler(nil)
    } catch {
      handler(error.localizedDescription)
    }
  }
  
  static func stopListenWithHandler() {
    if let authHandler = self.authHandler {
      FIRAuth.auth()?.removeStateDidChangeListener(authHandler)
      
      self.listenUserState(handler: nil)
    }
  }
  
  private static func setUserInformation(isLogged: Bool, name: String, avatar: URL?) {
    print(isLogged)
    if !isLogged {
      isUserLoggedIn = false
      username = ""
      userAvatar = nil
    } else {
      isUserLoggedIn = true
      username = name
      userAvatar = avatar
    }
  }
  
  private static func getReadableErrorString(error: Error) -> String {
    if error.localizedDescription == "There is no user record corresponding to this identifier. The user may have been deleted." || error.localizedDescription == "The password is invalid or the user does not have a password." {
      
      return "người dùng không tồn tại, có thể do sai email hoặc mật khẩu"
      
    } else if error.localizedDescription == "Network error (such as timeout, interrupted connection or unreachable host) has occurred." {
      return "kết nối mạng có vấn đề, xin hãy kiểm tra lại."
    } else {
      return "có vấn đề với việc đăng nhập, thử lại sau."
    }
  }
}
