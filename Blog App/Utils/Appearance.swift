//
//  Appearance.swift
//  Blog App
//
//  Created by Van Luu on 10/14/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

struct Appearance {
  static func fontForSize(size: CGFloat) -> UIFont {
    guard let font = UIFont(name: "AvenirNext-DemiBold", size: size) else {
      return UIFont.systemFont(ofSize: size)
    }
    
    return font
  }
  
  static func fontBoldForSize(size: CGFloat) -> UIFont {
    guard let font = UIFont(name: "AvenirNext-Bold", size: size) else {
      return UIFont.boldSystemFont(ofSize: size)
    }
    
    return font
  }
  
  static var attributeDetailText: [String : AnyObject] {
    var attribute = [String : AnyObject]()
    
    attribute = [
      NSFontAttributeName : Appearance.fontForSize(size: 15.0),
      NSForegroundColorAttributeName: AppColor.blogContent,
      NSParagraphStyleAttributeName : Appearance.paragraphStyle
    ]
    
    return attribute
  }
  
  static var attributeBoldText: [String : AnyObject] {
    var attribute = [String : AnyObject]()
    
    attribute = [
      NSFontAttributeName : Appearance.fontBoldForSize(size: 16.0),
      NSParagraphStyleAttributeName : Appearance.paragraphStyle
    ]
    
    return attribute
  }
  
  static var attributeTitleText: [String : AnyObject] {
    var attribute = [String : AnyObject]()
    
    attribute = [
      NSFontAttributeName : Appearance.fontBoldForSize(size: 18.0),
      NSForegroundColorAttributeName: AppColor.title,
      NSParagraphStyleAttributeName : Appearance.paragraphTitleStyle
    ]
    
    return attribute
  }
  
  static var attributeTimeText: [String : AnyObject] {
    var attribute = [String : AnyObject]()
    
    attribute = [
      NSFontAttributeName : Appearance.fontForSize(size: 15.0),
      NSForegroundColorAttributeName: AppColor.time,
      NSParagraphStyleAttributeName : Appearance.paragraphStyle
    ]
    
    return attribute
  }
  
  static var attributeNoteText: [String : AnyObject] {
    var attribute = [String : AnyObject]()
    
    attribute = [
      NSFontAttributeName : Appearance.fontForSize(size: 14.0),
      NSParagraphStyleAttributeName : Appearance.paragraphNoteStyle
    ]
    
    return attribute
  }
  
  static var attributeSummaryText: [String : AnyObject] {
    var attribute = [String : AnyObject]()
    
    attribute = [
      NSFontAttributeName : Appearance.fontForSize(size: 15.0),
      NSForegroundColorAttributeName: AppColor.blogContent,
      NSParagraphStyleAttributeName : Appearance.paragraphSummaryStyle
    ]
    
    return attribute
  }
  
  static var attributeSummaryTitleText: [String : AnyObject] {
    var attribute = [String : AnyObject]()
    
    attribute = [
      NSFontAttributeName : Appearance.fontForSize(size: 18.0),
      NSForegroundColorAttributeName: AppColor.blogContent,
      NSParagraphStyleAttributeName : Appearance.paragraphSummaryStyle
    ]
    
    return attribute
  }
  
  private static var paragraphStyle: NSMutableParagraphStyle {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineBreakMode = .byWordWrapping
    paragraphStyle.firstLineHeadIndent = 10.0
    paragraphStyle.headIndent = 10.0
    paragraphStyle.tailIndent = -10.0
    paragraphStyle.alignment = .justified
    paragraphStyle.lineSpacing = 5.0
    paragraphStyle.paragraphSpacing = 7.0
    
    return paragraphStyle
  }
  
  private static var paragraphTitleStyle: NSMutableParagraphStyle {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineBreakMode = .byWordWrapping
    paragraphStyle.firstLineHeadIndent = 10.0
    paragraphStyle.headIndent = 10.0
    paragraphStyle.tailIndent = -10.0
    paragraphStyle.alignment = .justified
    paragraphStyle.lineSpacing = 7.0
    paragraphStyle.paragraphSpacing = 5.0
    
    return paragraphStyle
  }
  
  private static var paragraphNoteStyle: NSMutableParagraphStyle {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineBreakMode = .byWordWrapping
    paragraphStyle.alignment = .center
    paragraphStyle.lineSpacing = 5.0
    
    return paragraphStyle
  }
  
  private static var paragraphSummaryStyle: NSMutableParagraphStyle {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.headIndent = 5.0
    paragraphStyle.tailIndent = -5.0
    paragraphStyle.lineBreakMode = .byWordWrapping
    paragraphStyle.firstLineHeadIndent = 5.0
    paragraphStyle.alignment = .left
    paragraphStyle.lineSpacing = 2.0
    
    return paragraphStyle
  }
  
  static func attributedStringWithTag(tags: (String, String), text: String) -> NSMutableAttributedString? {
    guard let attributeParam = attributeOuterStringForTag(tags) else { return nil }
    
    let attributeString = NSMutableAttributedString(string: text, attributes: attributeParam)
    
    do {
      let regex1 = try NSRegularExpression(pattern: tags.0, options: [])
      let regex2 = try NSRegularExpression(pattern: tags.1, options: [])
      var nsText = text as NSString
      
      let result1 = regex1.matches(in: text, options: [], range: NSRange(location: 0, length: nsText.length))
      let result2 = regex2.matches(in: text, options: [], range: NSRange(location: 0, length: nsText.length))
      
      for (index, tag) in result1.enumerated() {
        let startIndex = tag.range.location + 3
        let endIndex = result2[index].range.location
        let range = NSRange(location: startIndex, length: endIndex - startIndex)
        attributeString.addAttributes(attributeInterStringForTag(tags)!, range: range)
        
      }
      
      attributeString.mutableString.replaceOccurrences(of: tags.0, with: "", options: [], range: NSRange(location: 0, length: nsText.length))
      nsText = attributeString.mutableString as NSString
      attributeString.mutableString.replaceOccurrences(of: tags.1, with: "", options: [], range: NSRange(location: 0, length: nsText.length))
      
    } catch {}
    
    return attributeString
  }
  
  static func attributeOuterStringForTag(_ tags: (String, String)) -> [String : AnyObject]? {
    switch tags {
    case ("<b>", "</b>"):
      return Appearance.attributeDetailText
    case ("<t>", "</t>"):
      return Appearance.attributeTimeText
    default:
      return Appearance.attributeSummaryText
    }
  }
  
  static func attributeInterStringForTag(_ tags: (String, String)) -> [String : AnyObject]? {
    switch tags {
    case ("<b>", "</b>"):
      return Appearance.attributeBoldText
    case ("<t>", "</t>"):
      return Appearance.attributeTitleText
    default:
      return Appearance.attributeSummaryTitleText
    }
  }
}
