import UIKit
import Foundation

extension CGSize {
  static func aspectFitWithWidth(aspectRatio : CGSize, boundingSize: CGSize) -> CGFloat {
    return boundingSize.width * aspectRatio.height / aspectRatio.width
  }
  
  static func aspectFill(aspectRatio :CGSize, minimumSize: CGSize) -> CGSize {
    let mW = minimumSize.width / aspectRatio.width;
    let mH = minimumSize.height / aspectRatio.height;
    
    var returnSize = minimumSize
    
    if( mH > mW ) {
      returnSize.width = minimumSize.height / aspectRatio.height * aspectRatio.width;
    }
    else if( mW >= mH ) {
      returnSize.height = minimumSize.width / aspectRatio.width * aspectRatio.height;
    }

    return returnSize;
  }
}
