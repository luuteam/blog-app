//
//  String.swift
//  Blog App
//
//  Created by Van Luu on 10/14/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

extension String {
  func isMatchRegex(regex: String) -> Bool {
    let result = (self as NSString).range(of: regex, options: .regularExpression)
    
    if result.length == 0 {
      return false
    }
    
    return true
  }
  
  var cgFloat: CGFloat {
    if let number = NumberFormatter().number(from: self) {
      return CGFloat(number)
    }
    
    return 0.0
  }
  
  var isEmail: Bool {
    do {
      let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
      return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
    } catch {
      return false
    }
  }
  
  var isPasswordValid: Bool {
    if self != "" && (self as NSString).length > 8 {
      return true
    }
    
    return false
  }
  
  static func contains(_ text: String, substring: String,
                       ignoreCase: Bool = true,
                       ignoreDiacritic: Bool = true) -> Bool {
    
    var options = NSString.CompareOptions()
    
    if ignoreCase { _ = options.insert(NSString.CompareOptions.caseInsensitive) }
    if ignoreDiacritic { _ = options.insert(NSString.CompareOptions.diacriticInsensitive) }
    
    return text.range(of: substring, options: options) != nil
  }
}
