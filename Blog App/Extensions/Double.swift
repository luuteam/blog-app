//
//  Doubl.swift
//  Blog App
//
//  Created by Van Luu on 10/30/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import Foundation
import UIKit

extension Double {

  func timeFormat(formatter: DateFormatter) -> String {
    let date = Date(timeIntervalSince1970: self)
    return formatter.string(from: date)
  }
}
