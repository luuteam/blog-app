//
//  UIView.swift
//  Blog App
//
//  Created by Van Luu on 10/15/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

//@IBDesignable
extension UIView {
  
  @IBInspectable var cornerRadius: CGFloat {
    get {
      return self.layer.cornerRadius
    }
    set {
      self.layer.cornerRadius = newValue
    }
  }
  
  @IBInspectable var borderWidth: CGFloat {
    get {
      return self.layer.borderWidth
    }
    set {
      self.layer.borderWidth = newValue
    }
  }
  
  @IBInspectable var borderColor: UIColor {
    get {
      return UIColor.clear
    }
    set {
      self.layer.borderColor = newValue.cgColor
    }
  }
}

extension UIView {
//  public class func fromNib(nibNameOrNil: String? = nil) -> Self {
//    return fromNib(nibNameOrNil: nibNameOrNil, type: self)
//  }
//  
//  public class func fromNib<T : UIView>(nibNameOrNil: String? = nil, type: T.Type) -> UIView {
//    let v = fromNib(nibNameOrNil: nibNameOrNil, type: T.self)
//    return v!
//  }
//  
//  public class func fromNib<T : UIView>(nibNameOrNil: String? = nil, type: T.Type) -> UIView {
//    let name: String
//    if let nibName = nibNameOrNil {
//      name = nibName
//    } else {
//      // Most nibs are demangled by practice, if not, just declare string explicitly
//      name = nibName
//    }
//    
//    let nibView = Bundle.main.loadNibNamed(name, owner: nil, options: nil)?.first
//
//    return nibView as! UIView
//  }
//  
//  public class var nibName: String {
//    let name = NSStringFromClass(self).components(separatedBy: ".").last ?? ""
//    return name
//  }
//  public class var nib: UINib? {
//    if let _ = Bundle.main.path(forResource: nibName, ofType: "nib") {
//      return UINib(nibName: nibName, bundle: nil)
//    } else {
//      return nil
//    }
//  }
}
