//
//  UIViewController.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

extension UIStoryboard {
  func instantiateViewController<T:UIViewController>(type: T.Type) -> T? {
    var fullName: String = NSStringFromClass(T.self)
    if let range = fullName.range(of: ".", options: .backwards, range: nil, locale: nil) {
      fullName = fullName.substring(from: range.upperBound)
    }
    return self.instantiateViewController(withIdentifier: fullName) as? T
  }
}
