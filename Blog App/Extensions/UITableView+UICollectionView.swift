//
//  UITableView+UICollectionView.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit

extension UITableView {
  func dequeueReusableCell<T:UITableViewCell>(type: T.Type) -> T? {
    var fullName: String = NSStringFromClass(T.self)
    if let range = fullName.range(of: ".", options: .backwards, range: nil, locale: nil) {
      fullName = fullName.substring(from: range.upperBound)
    }
    return self.dequeueReusableCell(withIdentifier: fullName) as? T
  }
  
  func dequeueReusableCell<T:UITableViewCell>(type: T.Type, forIndexPath indexPath: IndexPath) -> T? {
    var fullName: String = NSStringFromClass(T.self)
    if let range = fullName.range(of: ".", options: .backwards, range: nil, locale: nil) {
      fullName = fullName.substring(from: range.upperBound)
    }
    return self.dequeueReusableCell(withIdentifier: fullName, for: indexPath) as? T
  }
  
  func registerCell<T: UITableViewCell>(type: T.Type) {
    var fullName: String = NSStringFromClass(T.self)
    if let range = fullName.range(of: ".", options: .backwards, range: nil, locale: nil) {
      fullName = fullName.substring(from: range.upperBound)
    }
    
    self.register(T.self, forCellReuseIdentifier: fullName)
  }
  
  func registerNib<T: UITableViewCell>(type: T.Type) {
    var fullName: String = NSStringFromClass(T.self)
    if let range = fullName.range(of: ".", options: .backwards, range: nil, locale: nil) {
      fullName = fullName.substring(from: range.upperBound)
    }
    
    let nib = UINib(nibName: fullName, bundle: nil)
    
    self.register(nib, forCellReuseIdentifier: fullName)
  }
}

extension UICollectionView {
  
  func dequeueReusableCell<T:UICollectionViewCell>(type: T.Type, forIndexPath indexPath: IndexPath) -> T? {
    var fullName: String = NSStringFromClass(T.self)
    if let range = fullName.range(of: ".", options: .backwards, range: nil, locale: nil) {
      fullName = fullName.substring(from: range.upperBound)
    }
    return self.dequeueReusableCell(withReuseIdentifier: fullName, for: indexPath) as? T
  }
  
  func registerCell<T: UICollectionViewCell>(type: T.Type) {
    var fullName: String = NSStringFromClass(T.self)
    if let range = fullName.range(of: ".", options: .backwards, range: nil, locale: nil) {
      fullName = fullName.substring(from: range.upperBound)
    }
    
    self.register(T.self, forCellWithReuseIdentifier: fullName)
  }
  
  func registerNib<T: UICollectionViewCell>(type: T.Type) {
    var fullName: String = NSStringFromClass(T.self)
    if let range = fullName.range(of: ".", options: .backwards, range: nil, locale: nil) {
      fullName = fullName.substring(from: range.upperBound)
    }
    
    let nib = UINib(nibName: fullName, bundle: nil)
    
    self.register(nib, forCellWithReuseIdentifier: fullName)
  }
}
