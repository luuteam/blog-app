//
//  AppDelegate.swift
//  Blog App
//
//  Created by Van Luu on 10/8/16.
//  Copyright © 2016 Van Luu. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    
    //for font in UIFont.familyNames {
    //print(UIFont.fontNames(forFamilyName: font))
    //}
    
    // configure IQKeyboardManager
    IQKeyboardManager.sharedManager().enable = true
    
    // Firebase configure
    FIRApp.configure()
    //
    // add listener to user state
    UserManager.listenUserState { isLoggedIn in
      self.handlerUserLoggedIn(isLoggedIn)
    }
    
    return true
  }
  
  private func handlerUserLoggedIn(_ isLoggedIn: Bool) {
    let rootVC = window!.rootViewController as! UINavigationController
    FlowManager.shareInstance.navigationController = rootVC
    FlowManager.shareInstance.setRootVC(userLoggedIn: isLoggedIn)
    
    // firebase capture handler block so first remove listener and listen again without handler
    UserManager.stopListenWithHandler()
  }
}

